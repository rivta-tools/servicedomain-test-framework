/*
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements. See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership. Sveriges Kommuner och Landsting licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License. You may obtain a copy of the License at
 http://www.apache.org/licenses/LICENSE-2.0
 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied. See the License for the
 specific language governing permissions and limitations
 under the License.
 */
package se.skl.rivta.tksupport.soapui.validation

import static org.junit.Assert.*

import org.apache.logging.log4j.*
import org.junit.Before
import org.junit.Test
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import se.skl.rivta.tksupport.soapui.UnitContextWrapper
import se.skl.rivta.tksupport.soapui.ContextHelperTK

import com.eviware.soapui.model.testsuite.TestCaseRunContext
import com.eviware.soapui.model.testsuite.TestRunContext
import com.eviware.soapui.support.GroovyUtils


class DocBookValidatorTest {

	Logger log = LogManager.getLogger(this.class)
	
	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();
	
	@Before
	void setUp() {
		log = LogManager.getLogger(this.class)
	}
	
	@Test
	void TEST_validateDocBook_PassWhenValidDocBookInOneRecord() {
		def contextHelper = getContextHelperCareDocumentation("response_GetCareDocumentation.xml")
		def allowedElements = ['article', 'info', 'title', 'para', 'section', 'bibliography']
		def validator = new DocBookValidator(contextHelper, allowedElements)
		def errors = validator.validateDocBook('/urn:careDocumentation/urn1:careDocumentationBody/urn1:clinicalDocumentNote/urn1:clinicalDocumentNoteText', false)
		log.info errors
		assertFalse(errors.any())
	}
	
	@Test
	void TEST_validateDocBook_FailWhenMissingDocBook() {
		def contextHelper = getContextHelperCareDocumentation("response_GetCareDocumentation.xml")
		def allowedElements = ['article', 'info', 'title', 'para', 'section', 'bibliography']
		def validator = new DocBookValidator(contextHelper, allowedElements)
		def errors = validator.validateDocBook('/urn:careDocumentation/urn1:careDocumentationBody/urn1:clinicalDocumentNote/urn1:clinicalDocumentNoteTitle', false)
		log.info errors
		assertTrue(errors.any())
	}
	
	@Test
	void TEST_validateDocBook_FailWhenInvalidDocBook() {
		def contextHelper = getContextHelperCareDocumentation("response_GetCareDocumentation-Errors.xml")
		def allowedElements = ['article', 'info', 'title', 'para', 'section', 'bibliography']
		def validator = new DocBookValidator(contextHelper, allowedElements)
		def errors = validator.validateDocBook('/urn:careDocumentation/urn1:careDocumentationBody/urn1:clinicalDocumentNote/urn1:clinicalDocumentNoteText', false)
		log.info errors
		assertTrue(errors.any())
	}
	
	@Test
	void TEST_validateDocBook_FailWhenInvalidElement() {
		def contextHelper = getContextHelperCareDocumentation("response_GetCareDocumentation.xml")
		def allowedElements = ['article', 'info', 'title']
		def validator = new DocBookValidator(contextHelper, allowedElements)
		def errors = validator.validateDocBook('/urn:careDocumentation/urn1:careDocumentationBody/urn1:clinicalDocumentNote/urn1:clinicalDocumentNoteText', false)
		log.info errors
		assertTrue(errors.any())
	}

	// Helper methods
	
	private ContextHelperTK getContextHelperCareDocumentation(String responseFileName) {
		def contextWrapper = new UnitContextWrapper()
		ContextHelperTK.setRecordElementPath("//urn:careDocumentation", contextWrapper)
		ContextHelperTK.setXmlNamespaces(["urn": "urn:riv:clinicalprocess:healthcond:description:GetCareDocumentationResponder:2",
										 "urn1": "urn:riv:clinicalprocess:healthcond:description:2"] ,  contextWrapper)
		contextWrapper.setProjectPath(this.getClass().getResource("").getPath())
		contextWrapper.setMessage(resourceToText(responseFileName))
		
		return new ContextHelperTK(contextWrapper, log)
	}

	private String resourceToText(String resName)
	{
		def uri = this.getClass().getResource(resName).toURI()
		def file = new File(uri)
		return file.text
	}
}
