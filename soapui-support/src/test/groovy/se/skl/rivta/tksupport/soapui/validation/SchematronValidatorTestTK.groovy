/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements. See the NOTICE file
distributed with this work for additional information
regarding copyright ownership. Sveriges Kommuner och Landsting licenses this file
to you under the Apache License, Version 2.0 (the
        "License"); you may not use this file except in compliance
with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied. See the License for the
specific language governing permissions and limitations
under the License.
*/
package se.skl.rivta.tksupport.soapui.validation

import org.apache.logging.log4j.*
import org.junit.Before
import org.junit.Test
import se.skl.rivta.tksupport.soapui.UnitContextWrapper

import static org.junit.Assert.assertEquals
/**
 *
 */
class SchematronValidatorTest {
	private UnitContextWrapper  contextWrapper2
	private Logger log = LogManager.getLogger(this.class)
	
    SchematronValidatorTK validator

    @Before
    void setup() {
        validator = new SchematronValidatorTK()
		log = LogManager.getLogger(this.class)
    }

    @Test
    void testValidateMessage() {

        def schematronFile = new File(this.class.getResource("constraints.xml").toURI())
        def message = new File(this.class.getResource("GetCareContacts.xml").toURI()).text
        def results = validator.validateMessage(message, schematronFile)
		results.each { result ->
			log.info "test: ${result.test} location: ${result.location} text: ${result.text}"
		}
        assertEquals("Should have one failure", 1, results.size())
    }

	@Test
	void testValidateInludes() {

		def schematronFile = new File(this.class.getResource("constraints-includes.xml").toURI())
		def message = new File(this.class.getResource("response_GetCareContacts.xml").toURI()).text
		def results = validator.validateMessage(message, schematronFile)
		results.each { result ->
			log.info "test: ${result.test} location: ${result.location} text: ${result.text}"
		}
		assertEquals("Should have two failures", 2, results.size())
	}
}
