package se.skl.rivta.tksupport.soapui

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse;

import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;

import com.eviware.soapui.impl.rest.RestRequestInterface.HttpMethod;
import com.eviware.soapui.model.iface.Attachment;
import com.eviware.soapui.model.mock.MockRequest
import com.eviware.soapui.model.mock.MockRunContext;
import com.eviware.soapui.support.types.StringToStringsMap;;

/**
 * Unit test stub for MockRequest interface. For unit testing of MockHelper class.
 */
class UnitMockRequest implements MockRequest {
	
	private requestContent
	
	public UnitMockRequest(String requestContent) {
		this.requestContent = requestContent;
	}

	@Override
	public XmlObject getContentElement() throws XmlException {
		// Auto-generated method stub
		return null;
	}

	@Override
	public MockRunContext getContext() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public HttpServletRequest getHttpRequest() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public HttpServletResponse getHttpResponse() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public HttpMethod getMethod() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public String getPath() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public String getProtocol() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public byte[] getRawRequestData() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public Attachment[] getRequestAttachments() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public String getRequestContent() {
		return this.requestContent;
	}

	@Override
	public MockRunContext getRequestContext() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public StringToStringsMap getRequestHeaders() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public XmlObject getRequestXmlObject() throws XmlException {
		// Auto-generated method stub
		return null;
	}

	@Override
	public void setRequestContent(String arg0) {
		// Auto-generated method stub

	}
}
