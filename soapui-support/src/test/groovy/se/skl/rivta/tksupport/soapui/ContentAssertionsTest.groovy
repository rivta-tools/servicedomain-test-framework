/*
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements. See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership. Sveriges Kommuner och Landsting licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License. You may obtain a copy of the License at
 http://www.apache.org/licenses/LICENSE-2.0
 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied. See the License for the
 specific language governing permissions and limitations
 under the License.
 */
package se.skl.rivta.tksupport.soapui

import static org.junit.Assert.*

import org.apache.logging.log4j.*
import org.junit.Before
import org.junit.Test
import org.junit.Rule
import org.junit.rules.TemporaryFolder

import com.eviware.soapui.model.testsuite.TestCaseRunContext
import com.eviware.soapui.model.testsuite.TestRunContext
import com.eviware.soapui.support.GroovyUtils


class ContentAssertionsTest {

	Logger log = LogManager.getLogger(this.class)
	
	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();
	
	@Before
	void setUp() {
		log = LogManager.getLogger(this.class)
	}

	@Test
	void TEST_assertElementExistsInAnyRecord_PassWhenPresentInOneRecord() {
		def contextWrapper = getContextWrapperCareDocumentation("response_GetCareDocumentation.xml")
		def ca = new ContentAssertions(contextWrapper, this.log)
		ca.assertElementExistsInAnyRecord('/urn:careDocumentation/urn1:careDocumentationBody/urn1:clinicalDocumentNote/urn1:clinicalDocumentNoteText')
	}
	
	@Test
	void TEST_assertElementExistsInAnyRecord_FailWhenNotPresent() {
		def contextWrapper = getContextWrapperCareDocumentation("response_GetCareDocumentation.xml")
		def ca = new ContentAssertions(contextWrapper, this.log)
		try {
			ca.assertElementExistsInAnyRecord('/urn:careDocumentation/urn1:careDocumentationHeader/urn1:accountableHealthcareProfessional/urn1:healthcareProfessionalRoleCode')
		}
		catch(AssertionError e) {
			log.info e.getMessage()
			return
		}
		fail("Expected AssertionError")
	}

	@Test
	void TEST_assertElementExistsInAnyRecord_FailWhenEmptyResponse() {
		def contextWrapper = getContextWrapperCareDocumentation("response_GetCareDocumentation-Empty.xml")
		def ca = new ContentAssertions(contextWrapper, this.log)
		try {
			ca.assertElementExistsInAnyRecord('/urn:careDocumentation/urn1:careDocumentationBody/urn1:clinicalDocumentNote/urn1:clinicalDocumentNoteText')
		}
		catch(AssertionError e) {
			log.info e.getMessage()
			return
		}
		fail("Expected AssertionError")
	}
	
	@Test
	void TEST_assertElementExistsInAllRecords_FailWhenMissingInOneRecord() {
		def contextWrapper = getContextWrapperCareDocumentation("response_GetCareDocumentation.xml")
		def ca = new ContentAssertions(contextWrapper, this.log)
		try {
			ca.assertElementExistsInAllRecords('/urn:careDocumentation/urn1:careDocumentationBody/urn1:clinicalDocumentNote/urn1:clinicalDocumentNoteText')
		}
		catch(AssertionError e) {
			log.info e.getMessage()
			return
		}
		fail("Expected AssertionError")
	}
	
	@Test
	void TEST_assertElementExistsInAllRecords_PassWhenPresentInAllRecords() {
		def contextWrapper = getContextWrapperCareDocumentation("response_GetCareDocumentation.xml")
		def ca = new ContentAssertions(contextWrapper, this.log)
		ca.assertElementExistsInAllRecords('/urn:careDocumentation/urn1:careDocumentationBody/urn1:clinicalDocumentNote')
	}
	
	@Test
	void TEST_assertElementExcludedInAnyRecord_PassWhenPresentInOneRecord() {
		def contextWrapper = getContextWrapperCareDocumentation("response_GetCareDocumentation.xml")
		def ca = new ContentAssertions(contextWrapper, this.log)
		ca.assertElementExcludedInAnyRecord('/urn:careDocumentation/urn1:careDocumentationBody/urn1:clinicalDocumentNote/urn1:clinicalDocumentNoteText')
	}
	
	@Test
	void TEST_assertElementExcludedInAnyRecord_FailWhenPresentInAllRecords() {
		def contextWrapper = getContextWrapperCareDocumentation("response_GetCareDocumentation.xml")
		def ca = new ContentAssertions(contextWrapper, this.log)
		try {
			ca.assertElementExcludedInAnyRecord('/urn:careDocumentation/urn1:careDocumentationBody/urn1:clinicalDocumentNote')
		}
		catch(AssertionError e) {
			log.info e.getMessage()
			return
		}
		fail("Expected AssertionError")
	}
	
	@Test
	void TEST_assertElementExcludedInAnyRecord_FailWhenEmptyResponse() {
		def contextWrapper = getContextWrapperCareDocumentation("response_GetCareDocumentation-Empty.xml")
		def ca = new ContentAssertions(contextWrapper, this.log)
		try {
			ca.assertElementExcludedInAnyRecord('/urn:careDocumentation/urn1:careDocumentationBody/urn1:clinicalDocumentNote/urn1:clinicalDocumentNoteText')
		}
		catch(AssertionError e) {
			log.info e.getMessage()
			return
		}
		fail("Expected AssertionError")
	}
	
	@Test
	void TEST_assertSpecificValueInAnyRecord_FailWhenNoMatch() {
		def contextWrapper = getContextWrapperCareDocumentation("response_GetCareDocumentation.xml")
		def ca = new ContentAssertions(contextWrapper, this.log)
		try {
			ca.assertSpecificValueInAnyRecord('/urn:careDocumentation/urn1:careDocumentationBody/urn1:clinicalDocumentNote/urn1:clinicalDocumentNoteCode', 'blaj')
		}
		catch(AssertionError e) {
			log.info e.getMessage()
			return
		}
		fail("Expected AssertionError")
	}
	
	@Test
	void TEST_assertSpecificValueInAnyRecord_PassWhenMatchInOneRecord() {
		def contextWrapper = getContextWrapperCareDocumentation("response_GetCareDocumentation.xml")
		def ca = new ContentAssertions(contextWrapper, this.log)
		ca.assertSpecificValueInAnyRecord('/urn:careDocumentation/urn1:careDocumentationBody/urn1:clinicalDocumentNote/urn1:clinicalDocumentNoteCode', 'utr')
	}
	
	@Test
	void TEST_assertSpecificValueInAnyRecord_PassWhenMatchInSecondElementInOneRecord() {
		def contextWrapper = getContextWrapperLaboratoryOrderOutcome("response_GetLaboratoryOrderOutcome.xml")
		def ca = new ContentAssertions(contextWrapper, this.log)
		ca.assertSpecificValueInAnyRecord('/urn:laboratoryOrderOutcome/urn1:laboratoryOrderOutcomeBody/urn1:analysis/urn1:analysisStatus', 'Planerad')
	}
	
	@Test
	void TEST_assertSpecificValueInAllRecords_PassWhenMatchInAllRecords() {
		def contextWrapper = getContextWrapperCareDocumentation("response_GetCareDocumentation.xml")
		def ca = new ContentAssertions(contextWrapper, this.log)
		ca.assertSpecificValueInAllRecords('/urn:careDocumentation/urn1:careDocumentationHeader/urn1:approvedForPatient', 'true')
	}
	
	@Test
	void TEST_assertSpecificValueInAllRecords_FailWhenMismatchInOne() {
		def contextWrapper = getContextWrapperCareDocumentation("response_GetCareDocumentation.xml")
		def ca = new ContentAssertions(contextWrapper, this.log)
		try {
			ca.assertSpecificValueInAllRecords('/urn:careDocumentation/urn1:careDocumentationBody/urn1:clinicalDocumentNote/urn1:clinicalDocumentNoteCode', 'utr')
		}
		catch(AssertionError e) {
			log.info e.getMessage()
			return
		}
		fail("Expected AssertionError")
	}

	@Test
	void TEST_assertSpecificValueInAllRecords_FailWhenMissingElementInOne() {
		def contextWrapper = getContextWrapperLaboratoryOrderOutcome("response_GetLaboratoryOrderOutcome.xml")
		def ca = new ContentAssertions(contextWrapper, this.log)
		try {
			ca.assertSpecificValueInAllRecords('/urn:laboratoryOrderOutcome/urn1:laboratoryOrderOutcomeBody/urn1:analysis/urn1:analysisStatus', 'Avklarad')
		}
		catch(AssertionError e) {
			log.info e.getMessage()
			return
		}
		fail("Expected AssertionError")
	}

	@Test
	void TEST_assertSpecificValueInAllRecords_FailWhenEmptyResponse() {
		def contextWrapper = getContextWrapperCareDocumentation("response_GetCareDocumentation-Empty.xml")
		def ca = new ContentAssertions(contextWrapper, this.log)
		try {
			ca.assertSpecificValueInAllRecords('/urn:careDocumentation/urn1:careDocumentationHeader/urn1:approvedForPatient', 'true')
		}
		catch(AssertionError e) {
			log.info e.getMessage()
			return
		}
		fail("Expected AssertionError")
	}
	
	@Test
	void TEST_assertValidValuesInAllRecords_PassWhenMatchInAllRecords() {
		def contextWrapper = getContextWrapperCareDocumentation("response_GetCareDocumentation.xml")
		def ca = new ContentAssertions(contextWrapper, this.log)
		ca.assertValidValuesInAllRecords('/urn:careDocumentation/urn1:careDocumentationBody/urn1:clinicalDocumentNote/urn1:clinicalDocumentNoteCode', ['sam', 'utr'])
	}
	
	@Test
	void TEST_assertValidValuesInAllRecords_FailWhenInvalidInOne() {
		def contextWrapper = getContextWrapperCareDocumentation("response_GetCareDocumentation.xml")
		def ca = new ContentAssertions(contextWrapper, this.log)
		try {
			ca.assertValidValuesInAllRecords('/urn:careDocumentation/urn1:careDocumentationBody/urn1:clinicalDocumentNote/urn1:clinicalDocumentNoteCode', ['utr'])
		}
		catch(AssertionError e) {
			log.info e.getMessage()
			return
		}
		fail("Expected AssertionError")
	}
	
	@Test
	void TEST_assertNoForbiddenValueInAnyRecord_PassWhenNotPresent() {
		def contextWrapper = getContextWrapperCareDocumentation("response_GetCareDocumentation.xml")
		def ca = new ContentAssertions(contextWrapper, this.log)
		ca.assertNoForbiddenValueInAnyRecord('/urn:careDocumentation/urn1:careDocumentationBody/urn1:clinicalDocumentNote/urn1:clinicalDocumentNoteCode', 'blaj')
	}
	
	@Test
	void TEST_assertNoForbiddenValueInAnyRecord_FailWhenMatchingInOne() {
		def contextWrapper = getContextWrapperLaboratoryOrderOutcome("response_GetLaboratoryOrderOutcome.xml")
		def ca = new ContentAssertions(contextWrapper, this.log)
		try {
			ca.assertNoForbiddenValueInAnyRecord('/urn:laboratoryOrderOutcome/urn1:laboratoryOrderOutcomeBody/urn1:analysis/urn1:analysisStatus', 'Planerad')
		}
		catch(AssertionError e) {
			log.info e.getMessage()
			return
		}
		fail("Expected AssertionError")
	}
	
	@Test
	void TEST_assertValidEncoding_PassWhenPrologOmitted() {
		def contextWrapper = getContextWrapperCareDocumentation("response_GetCareDocumentation.xml")
		def ca = new ContentAssertions(contextWrapper, this.log)
		ca.assertValidEncoding()
	}
	
	@Test
	void TEST_assertValidEncoding_PassWhenMatchingProlog() {
		def contextWrapper = getContextWrapperLaboratoryOrderOutcome("response_GetLaboratoryOrderOutcome.xml")
		def ca = new ContentAssertions(contextWrapper, this.log)
		ca.assertValidEncoding()
	}
	
	@Test
	void TEST_assertValidEncoding_FailWhenNonMatchingProlog() {
		def contextWrapper = getContextWrapperLaboratoryOrderOutcome("response_GetLaboratoryOrderOutcome-UTF16.xml")
		def ca = new ContentAssertions(contextWrapper, this.log)
		try {
			ca.assertValidEncoding()
		}
		catch(AssertionError e) {
			log.info e.getMessage()
			return
		}
		fail("Expected AssertionError")
	}
	
	@Test
	void TEST_assertBase64ContentInAnyElement_PassPresentInOneRecord() {
		def contextWrapper = getContextWrapperCareDocumentation("response_GetCareDocumentation.xml")
		def ca = new ContentAssertions(contextWrapper, this.log)
		ca.assertBase64ContentInAnyElement('/urn:careDocumentation/urn1:careDocumentationBody/urn1:clinicalDocumentNote/urn1:multimediaEntry/urn1:value')
	}
	
	@Test
	void TEST_assertBase64ContentInAnyElement_PassPresentInOneRecordAndSaveFile() {
		def contextWrapper = getContextWrapperCareDocumentation("response_GetCareDocumentation.xml")
		def ca = new ContentAssertions(contextWrapper, this.log)
		ca.assertBase64ContentInAnyElement('/urn:careDocumentation/urn1:careDocumentationBody/urn1:clinicalDocumentNote/urn1:multimediaEntry/urn1:value', 'embedded_file.png', 0, tempFolder.newFolder())
	}
	
	@Test
	void TEST_assertBase64ContentInAnyElement_FailWhenOverFileSizeLimit() {
		def contextWrapper = getContextWrapperCareDocumentation("response_GetCareDocumentation.xml")
		def ca = new ContentAssertions(contextWrapper, this.log)
		try {
			ca.assertBase64ContentInAnyElement('/urn:careDocumentation/urn1:careDocumentationBody/urn1:clinicalDocumentNote/urn1:multimediaEntry/urn1:value', null, 20)
		}
		catch(AssertionError e) {
			log.info e.getMessage()
			return
		}
		fail("Expected AssertionError")
	}
	
	@Test
	void TEST_assertElementStringLengthInAnyRange_PassWhenWithinLimit() {
		def contextWrapper = getContextWrapperCareDocumentation("response_GetCareDocumentation.xml")
		def ca = new ContentAssertions(contextWrapper, this.log)
		ca.assertElementStringLengthInAnyRange('/urn:careDocumentation/urn1:careDocumentationBody/urn1:clinicalDocumentNote/urn1:clinicalDocumentNoteCode', 2, 4)
		
	}
	
	private UnitContextWrapper getContextWrapperCareDocumentation(String responseFileName) {
		def contextWrapper = new UnitContextWrapper()
		ContextHelperTK.setRecordElementPath("//urn:careDocumentation", contextWrapper)
		ContextHelperTK.setXmlNamespaces(["urn": "urn:riv:clinicalprocess:healthcond:description:GetCareDocumentationResponder:2",
										 "urn1": "urn:riv:clinicalprocess:healthcond:description:2"] ,  contextWrapper)
		contextWrapper.setProjectPath(this.getClass().getResource("").getPath())
		contextWrapper.setMessage(resourceToText(responseFileName))
		
		return contextWrapper
	}
	
	private UnitContextWrapper getContextWrapperLaboratoryOrderOutcome(String responseFileName) {
		def contextWrapper = new UnitContextWrapper()
		ContextHelperTK.setRecordElementPath("//urn:laboratoryOrderOutcome", contextWrapper)
		ContextHelperTK.setXmlNamespaces(["urn": "urn:riv:clinicalprocess:healthcond:actoutcome:GetLaboratoryOrderOutcomeResponder:3",
										 "urn1": "urn:riv:clinicalprocess:healthcond:actoutcome:3", 
										 "urn2": "urn:riv:clinicalprocess:healthcond:actoutcome:3.1"] ,  contextWrapper)
		contextWrapper.setProjectPath(this.getClass().getResource("").getPath())
		contextWrapper.setMessage(resourceToText(responseFileName))
		
		return contextWrapper
	}

	private String resourceToText(String resName)
	{
		def uri = this.getClass().getResource(resName).toURI()
		def file = new File(uri)
		return file.text
	}
}
