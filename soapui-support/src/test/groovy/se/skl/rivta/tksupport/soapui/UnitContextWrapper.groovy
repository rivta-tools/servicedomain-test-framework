package se.skl.rivta.tksupport.soapui

import com.eviware.soapui.support.types.StringToStringsMap

class UnitContextWrapper implements ContextWrapper {

    def projectPath
    def message
    def properties = [:]
	def request
	def logTestData = false

    public void setProjectPath(String projectPath) {
	    this.projectPath = projectPath;
    }

    public void setMessage(String message) {
	    this.message = message;
    }
	
	public void setRequest(String request) {
		this.request = request;
	}
	
    public void setProperties(Map<String, String> propMap)
    {
	    propMap.each { key, value -> 
		    properties[key] = value
	    }
    }
    public setPropertyValue(String k, String value)
    {
	    properties[k] = value
    }
	
	public void setLogTestData(boolean logTestData) {
		this.logTestData = logTestData;
	}
    
    @Override
    public String getProjectPath() {
	    return projectPath;
    }
	
	@Override
	public String getContentType(){
		return "text/xml;charset=UTF-8"
	}
	
	@Override
	public String getRequest(){
		return this.request
	}
	
	@Override
	public byte[] getRawResponseData(){
		byte[] body = message.getBytes("UTF-8")
		int length = body.size()
		byte[] header = "HTTP/1.1 200 OK\nContent-Type: text/xml;charset=utf-8\nContent-Encoding: gzip\nContent-Length: ${length}\nServer: Jetty(6.1.26)\n\n".getBytes("US-ASCII")

		return [header, body].flatten()
	}
	
    @Override
    public String getMessage() {
	    return message
    }

    @Override
    public String getProperty(String property) {
	    return properties[property]
    }

    @Override
    public String getPropertyRecursive(String property) {
	    return getProperty(property)
    }

    @Override
    public Map<String, String> getTestCaseProperties() {
	    return properties
    }
	
	@Override
	public String getTestCaseLabel() {
		return "JUnit - Test"
	}
	
	@Override
	public String getSuitLabel() {
		return "JUnit - Test Suit"
	}
	
	@Override
	public String getProjectLabel() {
		return "JUnit - Test Project"
	}
	
	@Override
	public Long getExchangeTimestamp() {
		return System.currentTimeMillis()
	}
	
	@Override
	public Long getTimeTaken() {
		return 2324
	}
		
	@Override
	public StringToStringsMap getRequestHeaders() {
		return new StringToStringsMap(['Content-Type': ['text/xml;charset=UTF-8']])
	}

	@Override
	StringToStringsMap getResponseHeaders() {
		return new StringToStringsMap(['Content-Type': ['text/xml;charset=UTF-8']])
	}

	@Override
	public String getEndpoint() {
		return "Sample Endpoint"
	}

    @Override
    public Map<String, String> getProjectProperties() {
	    return properties
    }

    @Override
    public Map<String, String> getProperties() {
	    return properties
    }
	
	@Override
	public Boolean logTestData() {
		return logTestData
	}
	@Override
	public String logTestDataPath() {
		return "C:/temp/SOAP-UI/"
	}
	@Override
	public String logTestDataFilesAllowed() {
		return "400"
	}

	@Override
	public byte[] getRawRequestData() {
		byte[] body = request ? request.getBytes("UTF-8") : new byte[0]
		int length = body.size()
		byte[] header = "POST http://localhost:8088/endpoint HTTP/1.1\nContent-Type: text/xml;charset=UTF-8\nContent-Length: ${length}\nUser-Agent: Apache-HttpClient/4.1.1 (java 1.5)\n\n".getBytes("US-ASCII")

		return [header, body].flatten()
	}
	

}
