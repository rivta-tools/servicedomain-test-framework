package se.skl.rivta.tksupport.soapui

import org.apache.logging.log4j.*
import static org.junit.Assert.*;
import com.eviware.soapui.impl.wsdl.mock.WsdlMockRequest
import com.eviware.soapui.model.mock.MockRequest
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder


class MockHelperTest {
	
	Logger log = LogManager.getLogger(this.class)
	
	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder()

	@Test
	public void TEST_getEmptyElementsInRequest_NoEmpty() {
		def request = getMockRequest("request_GetCareContacts.xml")
		def helper = new MockHelper(request, null)
		def errors = helper.getEmptyElementsInRequest()
		log.info errors
		assertFalse(errors as boolean)
	}
	
	@Test
	public void TEST_getEmptyElementsInRequest_OnlyAttributes() {
		def request = getMockRequest("request_GetRequest.xml")
		def helper = new MockHelper(request, null)
		def errors = helper.getEmptyElementsInRequest()
		log.info errors
		assertFalse(errors as boolean)
	}
	
	@Test
	public void TEST_getEmptyElementsInRequest_EmptyExists() {
		def request = getMockRequest("request_GetCareContacts-emptyElement.xml")
		def helper = new MockHelper(request, null)
		def errors = helper.getEmptyElementsInRequest()
		log.info errors
		assertTrue(errors as boolean)
		assertTrue(errors.length() > 0)
	}
	
	@Test
	public void TEST_getSchematronErrorsInRequest_NoError() {
		def request = getMockRequest("request_GetCareContacts.xml")
		def helper = new MockHelper(request, null)
		def constraints = new File(getResourceURI("constraints.xml"))
		def errors = helper.getSchematronErrorsInRequest(constraints)
		log.info errors
		assertFalse(errors as boolean)
	}
	
	@Test
	public void TEST_getSchematronErrorsInRequest_HasError() {
		def request = getMockRequest("request_GetCareContacts-ConstraintError.xml")
		def helper = new MockHelper(request, null)
		def constraints = new File(getResourceURI("constraints.xml"))
		def errors = helper.getSchematronErrorsInRequest(constraints)
		log.info errors
		assertTrue(errors as boolean)
	}
	
	@Test
	public void TEST_storeRequestResponse() {
		def contextWrapper = getContextWrapper("request_GetCareContacts.xml", "response_GetCareDocumentation.xml")
		contextWrapper.setLogTestData(true)
		def helper = new MockHelper(contextWrapper, this.log)
		def filePath = contextWrapper.logTestDataPath()
		helper.storeRequestResponse()
	}
	
	// Helper methods
	
	private MockRequest getMockRequest(String fileName) {
		def message = resourceToText(fileName)
		return new UnitMockRequest(message)
	}
	
	private UnitContextWrapper getContextWrapper(String requestFileName, String responseFileName) {
		def contextWrapper = new UnitContextWrapper()
		contextWrapper.setProjectPath(this.getClass().getResource("").getPath())
		contextWrapper.setRequest(resourceToText(requestFileName))
		contextWrapper.setMessage(resourceToText(responseFileName))
		
		return contextWrapper
	}

	private String resourceToText(String resName)
	{
		def file = new File(getResourceURI(resName))
		return file.text
	}
	
	private URI getResourceURI(String resName) {
		return this.getClass().getResource(resName).toURI()
	}
}
