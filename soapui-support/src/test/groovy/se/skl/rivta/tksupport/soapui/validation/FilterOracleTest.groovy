package se.skl.rivta.tksupport.soapui.validation

import static org.junit.Assert.*

import org.apache.logging.log4j.*
import org.junit.Test

import se.skl.rivta.tksupport.soapui.UnitContextWrapper
import se.skl.rivta.tksupport.soapui.ContextHelperTK

class FilterOracleTest {
	
	Logger log = LogManager.getLogger(this.class)

	@Test
	void TEST_validateFilteredResponse_PassWhenExpectedEqual() {
		def contextHelper = getContextHelperCareContacts("response_GetCareContacts.xml")
		def allRecords = contextHelper.getContentRecords()
		def recordInfo = {record -> record.firstElementAt("/urn:careContact/urn1:careContactHeader/urn1:documentId")}
		def oracle = new FilterOracle(allRecords, { record -> true }, recordInfo)
		def errors = oracle.validateFilteredResponse(contextHelper)
		log.info errors
		assertFalse(errors.any())
	}
	
	@Test
	void TEST_validateFilteredResponse_FailWhenUnexpectedRecords() {
		def contextHelper = getContextHelperCareContacts("response_GetCareContacts.xml")
		def allRecords = contextHelper.getContentRecords()
		def recordInfo = {record -> record.firstElementAt("/urn:careContact/urn1:careContactHeader/urn1:documentId")}
		def oracle = new FilterOracle(allRecords, { record -> false }, recordInfo)
		def errors = oracle.validateFilteredResponse(contextHelper)
		log.info errors
		assertTrue(errors.any())
	}
	
	@Test
	void TEST_validateFilteredResponse_PassWhenExpectedFiltered() {
		def contextHelper1 = getContextHelperCareContacts("response_GetCareContacts.xml")
		def allRecords = contextHelper1.getContentRecords()
		def contextHelper2 = getContextHelperCareContacts("response_GetCareContacts-Filter.xml")
		def recordInfo = {record -> record.firstElementAt("/urn:careContact/urn1:careContactHeader/urn1:documentId")}
		def condition = { record -> record.firstElementAt("/urn:careContact/urn1:careContactHeader/urn1:accountableHealthcareProfessional/urn1:authorTime") == "20130631100000" }
		def oracle = new FilterOracle(allRecords, condition, recordInfo)
		def errors = oracle.validateFilteredResponse(contextHelper2)
		log.info errors
		assertFalse(errors.any())
	}
	
	@Test
	void TEST_validateFilteredResponse_FailWhenMissingRecords() {
		def contextHelper1 = getContextHelperCareContacts("response_GetCareContacts.xml")
		def allRecords = contextHelper1.getContentRecords()
		def contextHelper2 = getContextHelperCareContacts("response_GetCareContacts-Filter.xml")
		def recordInfo = {record -> record.firstElementAt("/urn:careContact/urn1:careContactHeader/urn1:documentId")}
		def condition = { record -> record.firstElementAt("/urn:careContact/urn1:careContactHeader/urn1:accountableHealthcareProfessional/urn1:authorTime")[0..3] == "2013" }
		def oracle = new FilterOracle(allRecords, condition, recordInfo)
		def errors = oracle.validateFilteredResponse(contextHelper2)
		log.info errors
		assertTrue(errors.any())
	}
	
	// Helper methods
	
	private ContextHelperTK getContextHelperCareContacts(String responseFileName) {
		def contextWrapper = new UnitContextWrapper()
		ContextHelperTK.setRecordElementPath("//urn:careContact", contextWrapper)
		ContextHelperTK.setXmlNamespaces(["urn": "urn:riv:clinicalprocess:logistics:logistics:GetCareContactsResponder:3",
										 "urn1": "urn:riv:clinicalprocess:logistics:logistics:3"] ,  contextWrapper)
		contextWrapper.setProjectPath(this.getClass().getResource("").getPath())
		contextWrapper.setMessage(resourceToText(responseFileName))
		
		return new ContextHelperTK(contextWrapper, log)
	}

	private String resourceToText(String resName)
	{
		def uri = this.getClass().getResource(resName).toURI()
		def file = new File(uri)
		return file.text
	}
}