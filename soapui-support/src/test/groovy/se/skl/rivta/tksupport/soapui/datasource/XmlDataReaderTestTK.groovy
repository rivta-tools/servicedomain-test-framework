/* 
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements. See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership. Sveriges Kommuner och Landsting licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License. You may obtain a copy of the License at
 http://www.apache.org/licenses/LICENSE-2.0
 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied. See the License for the
 specific language governing permissions and limitations
 under the License.
 */
package se.skl.rivta.tksupport.soapui.datasource

import static org.junit.Assert.*

import org.apache.logging.log4j.*
import org.junit.Before
import org.junit.Test
import org.junit.rules.TemporaryFolder

import se.skl.rivta.tksupport.soapui.ContextHelperTK

import com.eviware.soapui.model.testsuite.TestCaseRunContext
import com.eviware.soapui.model.testsuite.TestRunContext
import com.eviware.soapui.support.GroovyUtils

/**
 *
 */
class XmlDataReaderTest {

	XmlDataReaderTK dataSource
	TestRunContext context
	Map<String, String> resultMap
	Logger log = LogManager.getLogger(XmlDataReaderTest.class)
	
	@Before
	void setUp() {
		resultMap = [:]
		context = [setProperty: {key, value -> resultMap[key] = value }] as TestRunContext
		File dataFile = new File(this.class.getResource("data.xml").toURI())
		dataSource = new XmlDataReaderTK(context, dataFile)
	}

	@Test
	void testLoad() {
		dataSource.load("TestCase1")

		assertEquals("Wrong number of parameters", 8, resultMap.size())
		assertEquals("Wrong value of webServiceUrl", "http://localhost:9090/abc", resultMap["webServiceUrl"])
		assertEquals("Wrong value of careUnitHSAid", "123465", resultMap["careUnitHSAid"])
		assertEquals("Wrong value of overridden global value patientType", "2", resultMap["patientType"])
	}
}
