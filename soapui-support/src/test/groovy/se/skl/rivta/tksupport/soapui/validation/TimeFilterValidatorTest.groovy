/*
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements. See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership. Sveriges Kommuner och Landsting licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License. You may obtain a copy of the License at
 http://www.apache.org/licenses/LICENSE-2.0
 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied. See the License for the
 specific language governing permissions and limitations
 under the License.
 */
package se.skl.rivta.tksupport.soapui.validation

import static org.junit.Assert.*

import org.apache.logging.log4j.*
import org.junit.Before
import org.junit.Test
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import se.skl.rivta.tksupport.soapui.UnitContextWrapper
import se.skl.rivta.tksupport.soapui.ContextHelperTK

import com.eviware.soapui.model.testsuite.TestCaseRunContext
import com.eviware.soapui.model.testsuite.TestRunContext
import com.eviware.soapui.support.GroovyUtils


class TimeFilterValidatorTest {

	Logger log = LogManager.getLogger(this.class)
	
	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();
	
	@Before
	void setUp() {
		log = LogManager.getLogger(this.class)
	}
	
	@Test
	void TEST_validateTimestampInRange_PassWhenAllRecordsEqualOrInRange() {
		def contextHelper = getContextHelperCareDocumentation("response_GetCareDocumentation.xml")
		def validator = new TimeFilterValidator(contextHelper, '20160102112200', '20160102112233')
		def errors = validator.validateTimestampInRange('/urn:careDocumentation/urn1:careDocumentationHeader/urn1:accountableHealthcareProfessional/urn1:authorTime', false)
		log.info errors
		assertFalse(errors.any())
	}
	
	@Test
	void TEST_validateTimestampInRange_FailWhenRequiredAndWrongXpath() {
		def contextHelper = getContextHelperCareDocumentation("response_GetCareDocumentation.xml")
		def validator = new TimeFilterValidator(contextHelper, '20160102112200', '20160102112233')
		def errors = validator.validateTimestampInRange('/urn:careDocumentation/urn1:careDocumentationHeader/urn1:accountableHealthcareProfessional/urn1:wrongStuff', true)
		log.info errors
		assertTrue(errors.any())
	}
	
	@Test
	void TEST_validateTimestampInRange_PassWhenNotRequiredAndWrongXpath() {
		def contextHelper = getContextHelperCareDocumentation("response_GetCareDocumentation.xml")
		def validator = new TimeFilterValidator(contextHelper, '20160102112200', '20160102112233')
		def errors = validator.validateTimestampInRange('/urn:careDocumentation/urn1:careDocumentationHeader/urn1:accountableHealthcareProfessional/urn1:wrongStuff', false)
		log.info errors
		assertFalse(errors.any())
	}
	
	@Test
	void TEST_validateTimestampInRange_FailWhenOneRecordBefore() {
		def contextHelper = getContextHelperCareDocumentation("response_GetCareDocumentation.xml")
		def validator = new TimeFilterValidator(contextHelper, '20160102112211', '20160102112222')
		def errors = validator.validateTimestampInRange('/urn:careDocumentation/urn1:careDocumentationHeader/urn1:accountableHealthcareProfessional/urn1:authorTime', false)
		log.info errors
		assertTrue(errors.any())
	}
	
	@Test
	void TEST_validateTimestampInRange_PassWhenAllRecordsEqualYear() {
		def contextHelper = getContextHelperCareDocumentation("response_GetCareDocumentation.xml")
		def validator = new TimeFilterValidator(contextHelper, '2016', null)
		def errors = validator.validateTimestampInRange('/urn:careDocumentation/urn1:careDocumentationHeader/urn1:accountableHealthcareProfessional/urn1:authorTime', false)
		log.info errors
		assertFalse(errors.any())
	}
	
	@Test
	void TEST_validateTimestampInRange_FailWhenYearBefore() {
		def contextHelper = getContextHelperCareDocumentation("response_GetCareDocumentation.xml")
		def validator = new TimeFilterValidator(contextHelper, '2017', '2018')
		def errors = validator.validateTimestampInRange('/urn:careDocumentation/urn1:careDocumentationHeader/urn1:accountableHealthcareProfessional/urn1:authorTime', false)
		log.info errors
		assertTrue(errors.any())
	}
	
	@Test
	void TEST_validateTimestampInRange_FailWhenMonthAfter() {
		def contextHelper = getContextHelperCareDocumentation("response_GetCareDocumentation.xml")
		def validator = new TimeFilterValidator(contextHelper, null, '201512')
		def errors = validator.validateTimestampInRange('/urn:careDocumentation/urn1:careDocumentationHeader/urn1:accountableHealthcareProfessional/urn1:authorTime', false)
		log.info errors
		assertTrue(errors.any())
	}
	
	@Test
	void TEST_validateTimestampOutsideRange_FailWhenAllRecordsEqualOrInRange() {
		def contextHelper = getContextHelperCareDocumentation("response_GetCareDocumentation.xml")
		def validator = new TimeFilterValidator(contextHelper, '20160102112200', '20160102112233')
		def errors = validator.validateTimestampOutsideRange('/urn:careDocumentation/urn1:careDocumentationHeader/urn1:accountableHealthcareProfessional/urn1:authorTime', false)
		log.info errors
		assertTrue(errors.any())
	}
	
	@Test
	void TEST_validateTimestampOutsideRange_PassWhenBeforeWithOnlyStartOfRange() {
		def contextHelper = getContextHelperCareDocumentation("response_GetCareDocumentation.xml")
		def validator = new TimeFilterValidator(contextHelper, '2017', null)
		def errors = validator.validateTimestampOutsideRange('/urn:careDocumentation/urn1:careDocumentationHeader/urn1:accountableHealthcareProfessional/urn1:authorTime', false)
		log.info errors
		assertFalse(errors.any())
	}
	
	@Test
	void TEST_validateTimestampOutsideRange_PassWhenMissingAndNotRequired() {
		def contextHelper = getContextHelperCareDocumentation("response_GetCareDocumentation.xml")
		def validator = new TimeFilterValidator(contextHelper, '2017', null)
		def errors = validator.validateTimestampOutsideRange('/urn:careDocumentation/urn1:careDocumentationHeader/urn1:accountableHealthcareProfessional/urn1:anotherTime', false)
		log.info errors
		assertFalse(errors.any())
	}
	
	@Test
	void TEST_validateTimestampOutsideRange_FailWhenBeforeWithOnlyEndOfRange() {
		def contextHelper = getContextHelperCareDocumentation("response_GetCareDocumentation.xml")
		def validator = new TimeFilterValidator(contextHelper, null, '2017')
		def errors = validator.validateTimestampOutsideRange('/urn:careDocumentation/urn1:careDocumentationHeader/urn1:accountableHealthcareProfessional/urn1:authorTime', false)
		log.info errors
		assertTrue(errors.any())
	}
	
	@Test
	void TEST_validatePartOfTimeperiodInRange_PassWhenCompletelyInRange() {
		def contextHelper = getContextHelperCareContacts("response_GetCareContacts.xml")
		def validator = new TimeFilterValidator(contextHelper, '20130101', '20140101')
		def errors = validator.validatePartOfTimeperiodInRange('/urn:careContact/urn1:careContactBody/urn1:careContactTimePeriod/urn1:start',
															  '/urn:careContact/urn1:careContactBody/urn1:careContactTimePeriod/urn1:end', false, false)
		log.info errors
		assertFalse(errors.any())
	}
	
	@Test
	void TEST_validatePartOfTimeperiodInRange_FailWhenRequiredEndIsMissing() {
		def contextHelper = getContextHelperCareContacts("response_GetCareContacts.xml")
		def validator = new TimeFilterValidator(contextHelper, '20130101', '20140101')
		def errors = validator.validatePartOfTimeperiodInRange('/urn:careContact/urn1:careContactBody/urn1:careContactTimePeriod/urn1:start',
															  '/urn:careContact/urn1:careContactBody/urn1:careContactTimePeriod/urn1:wrongEnd', false, true)
		log.info errors
		assertTrue(errors.any())
	}
	
	@Test
	void TEST_validatePartOfTimeperiodInRange_FailWhenCompletelyBeforeRange() {
		def contextHelper = getContextHelperCareContacts("response_GetCareContacts.xml")
		def validator = new TimeFilterValidator(contextHelper, '2015', '2016')
		def errors = validator.validatePartOfTimeperiodInRange('/urn:careContact/urn1:careContactBody/urn1:careContactTimePeriod/urn1:start',
															  '/urn:careContact/urn1:careContactBody/urn1:careContactTimePeriod/urn1:end', false, false)
		log.info errors
		assertTrue(errors.any())
	}
	
	@Test
	void TEST_validatePartOfTimeperiodInRange_PassWhenPartiallyBeforeRange() {
		def contextHelper = getContextHelperCareContacts("response_GetCareContacts.xml")
		def validator = new TimeFilterValidator(contextHelper, '20130501', null)
		def errors = validator.validatePartOfTimeperiodInRange('/urn:careContact/urn1:careContactBody/urn1:careContactTimePeriod/urn1:start',
															  '/urn:careContact/urn1:careContactBody/urn1:careContactTimePeriod/urn1:end', false, false)
		log.info errors
		assertFalse(errors.any())
	}
	
	@Test
	void TEST_validateTimestampIsBefore_PassWhenBefore() {
		def contextHelper = getContextHelperCareContacts("response_GetCareContacts.xml")
		def validator = new TimeFilterValidator(contextHelper, '20130601', null)
		def errors = validator.validateTimestampIsBeforeRange('/urn:careContact/urn1:careContactBody/urn1:careContactTimePeriod/urn1:start', false)
		log.info errors
		assertFalse(errors.any())
	}
	
	@Test
	void TEST_validateTimestampIsBefore_FailWhenEqual() {
		def contextHelper = getContextHelperCareContacts("response_GetCareContacts.xml")
		def validator = new TimeFilterValidator(contextHelper, '2013', null)
		def errors = validator.validateTimestampIsBeforeRange('/urn:careContact/urn1:careContactBody/urn1:careContactTimePeriod/urn1:start', false)
		log.info errors
		assertTrue(errors.any())
	}
	
	@Test
	void TEST_validateTimestampIsBefore_FailWhenRequiredAndNotFound() {
		def contextHelper = getContextHelperCareContacts("response_GetCareContacts.xml")
		def validator = new TimeFilterValidator(contextHelper, '20130601', null)
		def errors = validator.validateTimestampIsBeforeRange('/urn:careContact/urn1:careContactBody/urn1:careContactTimePeriod/urn1:abc', true)
		log.info errors
		assertTrue(errors.any())
	}
	
	@Test
	void TEST_validateTimestampIsAfter_FailWhenBefore() {
		def contextHelper = getContextHelperCareContacts("response_GetCareContacts.xml")
		def validator = new TimeFilterValidator(contextHelper, null, '20130601')
		def errors = validator.validateTimestampIsAfterRange('/urn:careContact/urn1:careContactBody/urn1:careContactTimePeriod/urn1:start', false)
		log.info errors
		assertTrue(errors.any())
	}

	@Test
	void TEST_validateTimestampInRange_PassWhenAllRecordsEqualOrInRangeXSdateTimeInResponse() {
		def contextHelper = getContextHelperGetAccessLogsForPatient("response_GetAccessLogsForPatient.xml")
		def validator = new TimeFilterValidator(contextHelper, '20150102112200', '20170102112233')
		def errors = validator.validateTimestampInRange('/urn1:AccessLog/urn1:AccessDate', false)
		log.info errors
		assertFalse(errors.any())
	}
	
	@Test
	void TEST_validateTimestampInRange_PassWhenAllRecordsEqualOrInRangeXSdateTimeInLimits() {
		def contextHelper = getContextHelperGetAccessLogsForPatient("response_GetAccessLogsForPatient.xml")
		def validator = new TimeFilterValidator(contextHelper, '2015-08-01T00:00:00', '2016-10-11T11:22:33')
		def errors = validator.validateTimestampInRange('/urn1:AccessLog/urn1:AccessDate', false)
		log.info errors
		assertFalse(errors.any())
	}
	// Helper methods
	
	private ContextHelperTK getContextHelperCareDocumentation(String responseFileName) {
		def contextWrapper = new UnitContextWrapper()
		ContextHelperTK.setRecordElementPath("//urn:careDocumentation", contextWrapper)
		ContextHelperTK.setXmlNamespaces(["urn": "urn:riv:clinicalprocess:healthcond:description:GetCareDocumentationResponder:2",
										 "urn1": "urn:riv:clinicalprocess:healthcond:description:2"] ,  contextWrapper)
		contextWrapper.setProjectPath(this.getClass().getResource("").getPath())
		contextWrapper.setMessage(resourceToText(responseFileName))
		
		return new ContextHelperTK(contextWrapper, log)
	}
	
	private ContextHelperTK getContextHelperCareContacts(String responseFileName) {
		def contextWrapper = new UnitContextWrapper()
		ContextHelperTK.setRecordElementPath("//urn:careContact", contextWrapper)
		ContextHelperTK.setXmlNamespaces(["urn": "urn:riv:clinicalprocess:logistics:logistics:GetCareContactsResponder:3",
										 "urn1": "urn:riv:clinicalprocess:logistics:logistics:3"] ,  contextWrapper)
		contextWrapper.setProjectPath(this.getClass().getResource("").getPath())
		contextWrapper.setMessage(resourceToText(responseFileName))
		
		return new ContextHelperTK(contextWrapper, log)
	}
	
	private ContextHelperTK getContextHelperGetAccessLogsForPatient(String responseFileName) {
		def contextWrapper = new UnitContextWrapper()
		ContextHelperTK.setRecordElementPath("//urn:AccessLogsResultType/urn1:AccesssLogs/urn1:AccessLog", contextWrapper)
		ContextHelperTK.setXmlNamespaces(["urn": "urn:riv:ehr:log:querying:GetAccessLogsForPatientResponder:1",
										 "urn1": "urn:riv:ehr:log:querying:1"] ,  contextWrapper)
		contextWrapper.setProjectPath(this.getClass().getResource("").getPath())
		contextWrapper.setMessage(resourceToText(responseFileName))
		
		return new ContextHelperTK(contextWrapper, log)
	}
	private String resourceToText(String resName)
	{
		def uri = this.getClass().getResource(resName).toURI()
		def file = new File(uri)
		return file.text
	}
}
