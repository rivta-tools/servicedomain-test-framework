/*
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements. See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership. Sveriges Kommuner och Landsting licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License. You may obtain a copy of the License at
 http://www.apache.org/licenses/LICENSE-2.0
 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied. See the License for the
 specific language governing permissions and limitations
 under the License.
 */
package se.skl.rivta.tksupport.soapui

import static org.junit.Assert.*

import org.apache.logging.log4j.*
import org.junit.Before
import org.junit.Test
import org.junit.Rule
import org.junit.rules.TemporaryFolder

import com.eviware.soapui.model.testsuite.TestCaseRunContext
import com.eviware.soapui.model.testsuite.TestRunContext
import com.eviware.soapui.support.GroovyUtils


class ContextHelperTKTest {

	Logger log = LogManager.getLogger(this.class)
	
	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();
	
	@Before
	void setUp() {
		log = LogManager.getLogger(this.class)
	}
	
	@Test
	void TEST_logTestData() {
		def contextWrapper = getContextWrapperCareDocumentation("response_GetCareDocumentation.xml")
		contextWrapper.setLogTestData(true)
		def contextHelper = new ContextHelperTK(contextWrapper, log)
		contextHelper.logTestData()
	}

	@Test
	void TEST_validateForAllRecords_PassWhenAllTrue() {
		def contextWrapper = getContextWrapperCareDocumentation("response_GetCareDocumentation.xml")
		def contextHelper = new ContextHelperTK(contextWrapper, log)
		def errors = contextHelper.validateForAllRecords({ record -> true }, "This error should not happen")
		log.info errors
		assertFalse(errors.any())
	}
	
	@Test
	void TEST_validateForAllRecords_FailWhenAllFalse() {
		def contextWrapper = getContextWrapperCareDocumentation("response_GetCareDocumentation.xml")
		def contextHelper = new ContextHelperTK(contextWrapper, log)
		def errors = contextHelper.validateForAllRecords({ record -> false }, "This is an expected error message")
		log.info errors
		assertTrue(errors.any())
	}
	
	@Test
	void TEST_validateForAllRecords_firstElementAt_PassWhenEquals() {
		def contextWrapper = getContextWrapperCareDocumentation("response_GetCareDocumentation.xml")
		def contextHelper = new ContextHelperTK(contextWrapper, log)
		def errors = contextHelper.validateForAllRecords({ 
			record -> record.firstElementAt('/urn:careDocumentation/urn1:careDocumentationHeader/urn1:approvedForPatient') == 'true' 
		}, "approvedForPatient must be set to true")
		log.info errors
		assertFalse(errors.any())
	}
	
	@Test
	void TEST_validateForAllRecords_firstElementAt_PassWhenEqualsOrExcludedOrEmpty() {
		def contextWrapper = getContextWrapperCareDocumentation("response_GetCareDocumentation.xml")
		def contextHelper = new ContextHelperTK(contextWrapper, log)
		def errors = contextHelper.validateForAllRecords({ record -> 
			record.firstElementAt('/urn:careDocumentation/urn1:careDocumentationHeader/urn1:legalAuthenticator/urn1:legalAuthenticatorHSAId') in ['SE65469231332', null, '']
		}, "legalAuthenticatorHSAId should be SE65469231332 or excluded or empty")
		log.info errors
		assertFalse(errors.any())
	}
	
	@Test
	void TEST_validateForAllRecords_timestampIsBefore_FailWhenElementMissing() {
		def contextWrapper = getContextWrapperCareDocumentation("response_GetCareDocumentation.xml")
		def contextHelper = new ContextHelperTK(contextWrapper, log)
		def errors = contextHelper.validateForAllRecords({ record ->
			record.timestampIsBefore('/urn:careDocumentation/urn1:careDocumentationHeader/urn1:legalAuthenticator/urn1:wrongXpath', '20161212')
		}, "Wrong xPath should give this error")
		log.info errors
		assertTrue(errors.any())
	}
	
	@Test
	void TEST_validateForAllRecords_FailWhenNoRecords() {
		def contextWrapper = getContextWrapperCareDocumentation("response_GetCareDocumentation-Empty.xml")
		def contextHelper = new ContextHelperTK(contextWrapper, log)
		def errors = contextHelper.validateForAllRecords({ record -> true }, "This error should not happen if there are records")
		log.info errors
		assertTrue(errors.any())
	}
	
	@Test
	void TEST_validateForAnyRecord_firstElementAt_PassWhenOneEquals() {
		def contextWrapper = getContextWrapperCareDocumentation("response_GetCareDocumentation.xml")
		def contextHelper = new ContextHelperTK(contextWrapper, log)
		def errors = contextHelper.validateForAnyRecord({
			record -> record.firstElementAt('/urn:careDocumentation/urn1:careDocumentationBody/urn1:clinicalDocumentNote/urn1:multimediaEntry/urn1:mediaType') == 'text/xml'
		}, "Expected at least one mediaEntry with type 'text/xml'")
		log.info errors
		assertFalse(errors.any())
	}
	
	@Test
	void TEST_validateForAnyRecord_FailWhenAllFalse() {
		def contextWrapper = getContextWrapperCareDocumentation("response_GetCareDocumentation.xml")
		def contextHelper = new ContextHelperTK(contextWrapper, log)
		def errors = contextHelper.validateForAnyRecord({ record -> false }, "This is an expected error message")
		log.info errors
		assertTrue(errors.any())
	}
	
	@Test
	void TEST_validateForAnyRecord_FailWhenNoRecords() {
		def contextWrapper = getContextWrapperCareDocumentation("response_GetCareDocumentation-Empty.xml")
		def contextHelper = new ContextHelperTK(contextWrapper, log)
		def errors = contextHelper.validateForAnyRecord({ record -> true }, "This error should not happen if there are records")
		log.info errors
		assertTrue(errors.any())
	}
	
	@Test
	void TEST_validateForAnyRecord_allElementsAt_PassWhenOneEquals() {
		def contextWrapper = getContextWrapperLaboratoryOrderOutcome("response_GetLaboratoryOrderOutcome.xml")
		def contextHelper = new ContextHelperTK(contextWrapper, log)
		def errors = contextHelper.validateForAnyRecord({
			record -> 'Planerad' in record.allElementsAt('/urn:laboratoryOrderOutcome/urn1:laboratoryOrderOutcomeBody/urn1:analysis/urn1:analysisStatus')
		}, "Expected at least one analysis with status 'Avklarad'")
		log.info errors
		assertFalse(errors.any())
	}
	
	@Test
	void TEST_validateForAnyRecord_timestampInRange_PassWhenOneInRange() {
		def contextWrapper = getContextWrapperCareDocumentation("response_GetCareDocumentation.xml")
		def contextHelper = new ContextHelperTK(contextWrapper, log)
		def authorTime = '/urn:careDocumentation/urn1:careDocumentationHeader/urn1:accountableHealthcareProfessional/urn1:authorTime'
		def errors = contextHelper.validateForAnyRecord({ 
			record -> record.timestampInRange(authorTime, '20160102', '20160102')
		}, "Expected authorTime in time range")
		log.info errors
		assertFalse(errors.any())
	}
	
	@Test
	void TEST_validateForAnyRecord_timestampInRange_FailWhenAllOutsideRange() {
		def contextWrapper = getContextWrapperCareDocumentation("response_GetCareDocumentation.xml")
		def contextHelper = new ContextHelperTK(contextWrapper, log)
		def authorTime = '/urn:careDocumentation/urn1:careDocumentationHeader/urn1:accountableHealthcareProfessional/urn1:authorTime'
		def errors = contextHelper.validateForAnyRecord({
			record -> record.timestampInRange(authorTime, '20160106', '20160108')
		}, "Expected authorTime in time range")
		log.info errors
		assertTrue(errors.any())
	}
	
	@Test
	void TEST_validateForAnyRecord_timestampIsBefore_PassWhenOneBefore() {
		def contextWrapper = getContextWrapperCareDocumentation("response_GetCareDocumentation.xml")
		def contextHelper = new ContextHelperTK(contextWrapper, log)
		def authorTime = '/urn:careDocumentation/urn1:careDocumentationHeader/urn1:accountableHealthcareProfessional/urn1:authorTime'
		def errors = contextHelper.validateForAnyRecord({
			record -> record.timestampIsBefore(authorTime, '20160103')
		}, "Expected authorTime to be before limit")
		log.info errors
		assertFalse(errors.any())
	}
	
	@Test
	void TEST_validateForAnyRecord_timestampIsAfter_FailWhenAllEqualOrBefore() {
		def contextWrapper = getContextWrapperCareDocumentation("response_GetCareDocumentation.xml")
		def contextHelper = new ContextHelperTK(contextWrapper, log)
		def authorTime = '/urn:careDocumentation/urn1:careDocumentationHeader/urn1:accountableHealthcareProfessional/urn1:authorTime'
		def errors = contextHelper.validateForAnyRecord({
			record -> record.timestampIsAfter(authorTime, '20160104')
		}, "Expected authorTime to be after limit")
		log.info errors
		assertTrue(errors.any())
	}
	
	@Test
	void TEST_validateForListOfRecords_PassForCorrectSize() {
		def contextWrapper = getContextWrapperCareDocumentation("response_GetCareDocumentation.xml")
		def contextHelper = new ContextHelperTK(contextWrapper, log)
		def errors = contextHelper.validateForListOfRecords({ records -> records.size() == 3 }, "Expected 3 records")
		log.info errors
		assertFalse(errors.any())
	}
	
	@Test
	void TEST_validateForListOfRecords_PassForUniqueCount() {
		def contextWrapper = getContextWrapperCareDocumentation("response_GetCareDocumentation.xml")
		def contextHelper = new ContextHelperTK(contextWrapper, log)
		def errors = contextHelper.validateForListOfRecords({ records -> 
			records.unique{r -> r.firstElementAt('/urn:careDocumentation/urn1:careDocumentationBody/urn1:clinicalDocumentNote/urn1:clinicalDocumentNoteCode') }.size() == 2 
		}, "Expected 2 unique note codes")
		log.info errors
		assertFalse(errors.any())
	}
	
	@Test
	void TEST_validateForListOfRecords_FailForUniqueCount() {
		def contextWrapper = getContextWrapperCareDocumentation("response_GetCareDocumentation.xml")
		def contextHelper = new ContextHelperTK(contextWrapper, log)
		def errors = contextHelper.validateForListOfRecords({ records ->
			records.unique{r -> r.firstElementAt('/urn:careDocumentation/urn1:careDocumentationBody/urn1:clinicalDocumentNote/urn1:clinicalDocumentNoteCode') }.size() == 3
		}, "This is an expected error message")
		log.info errors
		assertTrue(errors.any())
	}
	
	private UnitContextWrapper getContextWrapperCareDocumentation(String responseFileName) {
		def contextWrapper = new UnitContextWrapper()
		ContextHelperTK.setRecordElementPath("//urn:careDocumentation", contextWrapper)
		ContextHelperTK.setXmlNamespaces(["urn": "urn:riv:clinicalprocess:healthcond:description:GetCareDocumentationResponder:2",
										 "urn1": "urn:riv:clinicalprocess:healthcond:description:2"] ,  contextWrapper)
		contextWrapper.setProjectPath(this.getClass().getResource("").getPath())
		contextWrapper.setMessage(resourceToText(responseFileName))
		
		return contextWrapper
	}
	
	private UnitContextWrapper getContextWrapperLaboratoryOrderOutcome(String responseFileName) {
		def contextWrapper = new UnitContextWrapper()
		ContextHelperTK.setRecordElementPath("//urn:laboratoryOrderOutcome", contextWrapper)
		ContextHelperTK.setXmlNamespaces(["urn": "urn:riv:clinicalprocess:healthcond:actoutcome:GetLaboratoryOrderOutcomeResponder:3",
										 "urn1": "urn:riv:clinicalprocess:healthcond:actoutcome:3", 
										 "urn2": "urn:riv:clinicalprocess:healthcond:actoutcome:3.1"] ,  contextWrapper)
		contextWrapper.setProjectPath(this.getClass().getResource("").getPath())
		contextWrapper.setMessage(resourceToText(responseFileName))
		
		return contextWrapper
	}

	private String resourceToText(String resName)
	{
		def uri = this.getClass().getResource(resName).toURI()
		def file = new File(uri)
		return file.text
	}
}
