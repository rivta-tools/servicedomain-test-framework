<?xml version="1.0" encoding="utf-8"?>
<!--
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements. See the NOTICE file
distributed with this work for additional information
regarding copyright ownership. Sveriges Kommuner och Landsting licenses this file
to you under the Apache License, Version 2.0 (the
        "License"); you may not use this file except in compliance
with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied. See the License for the
specific language governing permissions and limitations
under the License.
-->
<iso:schema
        xmlns="http://purl.oclc.org/dsdl/schematron"
        xmlns:iso="http://purl.oclc.org/dsdl/schematron"
        queryBinding='xslt2'
        schemaVersion='ISO19757-3'>

    <iso:title>Validation for GetCareContacts</iso:title>
    <iso:ns prefix='urn2' uri='urn:riv:clinicalprocess:logistics:logistics:3.1'/>
    <iso:ns prefix='urn1' uri='urn:riv:clinicalprocess:logistics:logistics:3'/>
    <iso:ns prefix='urn' uri='urn:riv:clinicalprocess:logistics:logistics:GetCareContactsResponder:3'/>
	<iso:ns prefix='soapenv' uri='http://schemas.xmlsoap.org/soap/envelope/'/>

	<iso:include href="common-patterns.xml"/>

    <!-- Implementing abstract patterns -->

	<iso:pattern id="Verify healthcareProfessionalRoleCode" is-a="pattern.CvType">
		<iso:param name="path" value="urn1:healthcareProfessionalRoleCode" />
    </iso:pattern>
	<iso:pattern id="Verify careContactCode" is-a="pattern.CvType">
		<iso:param name="path" value="urn1:careContactCode" />
    </iso:pattern>
	<iso:pattern id="Verify careContactStatus" is-a="pattern.CvType">
		<iso:param name="path" value="urn1:careContactStatus" />
    </iso:pattern>
	<iso:pattern id="Verify gender" is-a="pattern.CvType">
		<iso:param name="path" value="urn1:gender" />
    </iso:pattern>
	<iso:pattern id="Verify physicalType" is-a="pattern.CvType">
		<iso:param name="path" value="urn1:physicalType" />
    </iso:pattern>

	<!-- Rules for elements -->

    <iso:pattern id="Verify dateOfBirth">
        <iso:rule context="urn1:dateOfBirth">
            <iso:assert test="string-length(urn1:value) = string-length(urn1:format)"><iso:name /> must have matching format and value elements.</iso:assert>
        </iso:rule>
    </iso:pattern>

	<iso:pattern id="Verify resultCode">
        <iso:rule context="urn:result">
            <assert test="(urn1:resultCode = 'ERROR' and urn1:errorCode) or urn1:resultCode != 'ERROR'">If resultCode is ERROR, errorCode must be given.</assert>
        </iso:rule>
    </iso:pattern>

	<iso:pattern id="Verifiera status mot tidsperiod">
        <iso:rule context="urn1:careContactStatus/urn1:code">
            <assert test="doc('codes.xml')//code[.=current()]">Felaktig statuskod</assert> 
        </iso:rule>
    </iso:pattern>

	<iso:pattern id="Verify location">
        <iso:rule context="urn1:careContactBody/urn2:location">
			<assert test="../urn1:careContactCode/urn1:code = 3">careContactBody/location can only exist if careContactCode is 3 (vårdtillfälle).</assert>
        </iso:rule>
    </iso:pattern>

	<iso:pattern id="Verify period">
        <iso:rule context="urn1:careContactBody/urn2:location/urn2:period">
			<assert test="count(urn1:start) + count(urn1:end) > 0">At least one of start or end must be given if location/period is given.</assert>
        </iso:rule>
    </iso:pattern>


	<!-- Fields that MUST NOT exist -->
	<iso:pattern id="No forbidden elements">
		<iso:rule context="urn1:careContactHeader">
			<iso:assert test="not(urn1:documentTitle)">urn1:documentTitle MUST NOT exist in <iso:name/></iso:assert>
			<iso:assert test="not(urn1:documentTime)">urn1:documentTime MUST NOT exist in <iso:name/></iso:assert>
			<iso:assert test="not(urn1:legalAuthenticator)">urn1:legalAuthenticator MUST NOT exist in <iso:name/></iso:assert>
			<iso:assert test="not(urn1:nullified)">urn1:nullified MUST NOT exist in <iso:name/></iso:assert>
			<iso:assert test="not(urn1:nullifiedReason)">urn1:nullifiedReason MUST NOT exist in <iso:name/></iso:assert>
			<iso:assert test="not(urn1:careContactId)">urn1:careContactId MUST NOT exist in <iso:name/></iso:assert>
		</iso:rule>
		<iso:rule context="urn1:careContactBody/urn1:additionalPatientInformation/urn1:gender">
			<iso:assert test="not(urn1:originalText)">urn1:originalText MUST NOT exist in urn1:additionalPatientInformation/<iso:name/></iso:assert>
		</iso:rule>
		<iso:rule context="urn1:careContactBody/urn2:location/urn2:location">
			<iso:assert test="not(urn2:status)">urn2:status MUST NOT exist in <iso:name/></iso:assert>
			<iso:assert test="not(urn2:operationalStatus)">urn2:operationalStatus MUST NOT exist in <iso:name/></iso:assert>
			<iso:assert test="not(urn2:alias)">urn2:alias MUST NOT exist in <iso:name/></iso:assert>
			<iso:assert test="not(urn2:description)">urn2:description MUST NOT exist in <iso:name/></iso:assert>
			<iso:assert test="not(urn2:mode)">urn2:mode MUST NOT exist in <iso:name/></iso:assert>
			<iso:assert test="not(urn2:type)">urn2:type MUST NOT exist in <iso:name/></iso:assert>
			<iso:assert test="not(urn2:telecom)">urn2:telecom MUST NOT exist in <iso:name/></iso:assert>
			<iso:assert test="not(urn2:partOf)">urn2:partOf MUST NOT exist in <iso:name/></iso:assert>
			<iso:assert test="not(urn2:managingOrganization/urn1:orgUnitLocation)">urn2:managingOrganization/urn1:orgUnitLocation MUST NOT exist in <iso:name/></iso:assert>
		</iso:rule>
	</iso:pattern>

	<!-- Fields that MUST exist -->
	<iso:pattern id="Mandatory elements">
		<iso:rule context="urn1:careContactBody/urn2:location">
			<iso:assert test="urn2:status">urn2:status MUST exist in <iso:name/></iso:assert>
		</iso:rule>
	</iso:pattern>


	<iso:pattern id="Verify non-empty elements">
        <iso:rule context="soapenv:Body/urn:GetCareContactsResponse//*">
		    <iso:assert test="normalize-space(.)">Element <iso:name /> is included but empty. All elements included in the response must have valid values.</iso:assert>
        </iso:rule>
    </iso:pattern>

		<iso:pattern id="Verify unique recordId">
		<iso:rule context="urn:GetCareContactsResponse">
			<let name="recordIdPath" value="urn:careContact/urn1:careContactHeader/urn1:documentId"/>
			<iso:assert test="count($recordIdPath) = count(distinct-values($recordIdPath))">Each record must have a unique documentId.</iso:assert>
		</iso:rule>
	</iso:pattern>

</iso:schema>
