/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements. See the NOTICE file
distributed with this work for additional information
regarding copyright ownership. Sveriges Kommuner och Landsting licenses this file
to you under the Apache License, Version 2.0 (the
        "License"); you may not use this file except in compliance
with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied. See the License for the
specific language governing permissions and limitations
under the License.
*/
package se.skl.rivta.tksupport.soapui.datasource

import com.eviware.soapui.model.mock.MockRunContext
import com.eviware.soapui.support.GroovyUtils
import org.apache.logging.log4j.*

/**
 * DataSource used to add mock parameters from an external xml file.
 */
class XmlDataReaderMock {

    private Logger log = LogManager.getLogger(this.class)

    MockRunContext context
    File dataFile
	
	/**
	 * Loads parameters from data file to mock context.
	 *
	 * @param the context object to load the parameters into
	 * @param dataFile the data file (File object)
	 */
	public XmlDataReaderMock(MockRunContext context, File dataFile) {
		this.context = context
		this.dataFile = dataFile

		if (!dataFile.exists()) {
			throw new IllegalArgumentException("dataFile does not exist: " + dataFile.absolutePath)
		}
	}
	
	/**
	 * Loads parameters from data file to mock context.
	 *
	 * @param the context object to load the parameters into
	 * @param dataFileName the data file name, relative to project path (default is data.xml)
	 */
	public XmlDataReaderMock(MockRunContext context, String dataFileName = "data.xml") {
		this(context, new File(new GroovyUtils(context).projectPath, dataFileName))
	}

    public void load() {
        def mockService = new XmlParser().parse(dataFile)

        mockService.globaldata[0].each { child -> context.setProperty(child.name(), child.text()) }
    }

}
