/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements. See the NOTICE file
distributed with this work for additional information
regarding copyright ownership. Sveriges Kommuner och Landsting licenses this file
to you under the Apache License, Version 2.0 (the
        "License"); you may not use this file except in compliance
with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied. See the License for the
specific language governing permissions and limitations
under the License.
*/
package se.skl.rivta.tksupport.soapui.validation

import net.sf.saxon.Configuration
import net.sf.saxon.TransformerFactoryImpl
import org.apache.logging.log4j.*

import com.eviware.soapui.model.testsuite.TestCaseRunContext;
import com.eviware.soapui.support.GroovyUtils
import java.util.List;

import javax.xml.transform.Source
import javax.xml.transform.TransformerException
import javax.xml.transform.TransformerFactory
import javax.xml.transform.URIResolver
import javax.xml.transform.stream.StreamResult
import javax.xml.transform.stream.StreamSource

/**
 * Validator used to verify soap messages according to Schematron rules.
 */
class SchematronValidatorTK {

    private Logger log = LogManager.getLogger(SchematronValidatorTK.class)
	private def path
    private TransformerFactory transformerFactory
	
    public SchematronValidatorTK() {
		def config = new Configuration()
		// Use strict policy to catch missing include files
		config.setRecoveryPolicy(Configuration.DO_NOT_RECOVER)
		transformerFactory = new TransformerFactoryImpl(config)
    }
	

	
/**
 * Validates the response message against the schematron file and returns a list of faults.
 * @param message - message to be validated in string format.
 * @param schematronFile - file containing schematron rules.
 * @return - list of fails.
 */
    public List<ValidationFailure> validateMessage(String message, File schematronFile) {

        transformerFactory.setURIResolver(new CustomUriResolver(schematronFile.getParent()))

        def gatheredConstraints = gatherContraints(new StreamSource(schematronFile))

        def expandedConstraints = expandContraints(stringSource(gatheredConstraints))

        def rules = convertToRules(stringSource(expandedConstraints))

        def validationResults = validateDocument(stringSource(message), stringSource(rules))

        def resultDoc = new XmlSlurper().parseText(validationResults)

        def failedAsserts = resultDoc.breadthFirst().findAll { child -> child.name() == "failed-assert" }

        def failures = new ArrayList<ValidationFailure>()
        failedAsserts.each { node -> failures.add(new ValidationFailure(test: node.@test, location: node.@location, text: node.text.text())) }

        failures
    }

    private String gatherContraints(Source constraints) {
        transform(source("iso_dsdl_include.xsl"), constraints)
    }

    private String expandContraints(Source gatheredConstraints) {
        transform(source("iso_abstract_expand.xsl"), gatheredConstraints)
    }

    private String convertToRules(Source expandedConstraints) {
        transform(source("iso_svrl_for_xslt2.xsl"), expandedConstraints)
    }

    private String validateDocument(Source document, Source rules) {
        transform(rules, document)
    }

    String transform(Source templateSource, Source documentSource) {
        def transformer = transformerFactory.newTransformer(templateSource)
        def result = new StringResult()
        transformer.transform(documentSource, result)

        result.toString()
    }

    Source source(path) {
        def resource = this.getClass().getResourceAsStream(path)
        //log.debug("Resource for path : " + path )
        //log.debug("Resource for resource : " + resource)
        new StreamSource(resource)
    }

    Source stringSource(contents) {
        new StreamSource(new StringReader(contents))
    }

    static class ValidationFailure {
        String test
        String location
        String text
    }

    static class StringResult extends StreamResult {
        StringWriter writer

        StringResult() {
            super()
            writer = new StringWriter()
            setWriter(writer)
        }

        @Override
        String toString() {
            writer.toString()
        }
    }
	
	private class CustomUriResolver implements URIResolver {
		private String schematronDir

		public CustomUriResolver(String schematronDir) {
			this.schematronDir = schematronDir
		}

		@Override
		Source resolve(String href, String base) throws TransformerException {
			log.debug("Resolve " + href + ", " + base)
			// Fetch Schematron transformation files as resources
			def resource = SchematronValidatorTK.class.getResourceAsStream(href)
			if (resource) {
				return new StreamSource(resource)
			}
			// Fetch references from constraints file
			def includeFile = new File(schematronDir, href)
			if (includeFile.exists()) {
				return new StreamSource(includeFile)
			}
			throw new TransformerException("File $href not found")
		}
	}

}
