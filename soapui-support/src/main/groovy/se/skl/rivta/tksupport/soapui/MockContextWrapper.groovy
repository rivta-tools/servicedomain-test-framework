package se.skl.rivta.tksupport.soapui

import java.util.Map

import com.eviware.soapui.support.GroovyUtils
import com.eviware.soapui.model.mock.MockResult
import com.eviware.soapui.model.mock.MockRunContext
import com.eviware.soapui.model.mock.MockService
import com.eviware.soapui.model.project.Project
import com.eviware.soapui.model.testsuite.TestStepResult
import com.eviware.soapui.model.testsuite.TestCase
import com.eviware.soapui.model.testsuite.TestProperty
import com.eviware.soapui.support.types.StringToObjectMap
import com.eviware.soapui.support.types.StringToStringsMap

class MockContextWrapper implements ContextWrapper {
    
    MockRunContext context
    MockResult mockResult
    private log
    
    public MockContextWrapper(MockRunContext context, MockResult mockResult, log)
    {
	    this.context = context
	    this.mockResult = mockResult
        this.log = log
    }

	
	@Override 
	public Long getExchangeTimestamp()
	{
		return mockResult.getTimestamp()	
	}
	
	@Override
	public Long getTimeTaken() {
		return mockResult.getTimeTaken()
	}
	@Override
	public String getEndpoint() {
		return context.getMockService().getLocalEndpoint()
	}
	
    @Override
    public String getProjectPath()
    {
        def path = new GroovyUtils(context).getProjectPath()

        if (path != "") {
            return path
        } else {
            return "."
        }
    }

	@Override
	public String getContentType(){
		// One of these may be null, possibly different between SoapUI versions
		def response = context.getMockResponse() ?: mockResult.getMockResponse()
		return response.getContentType().toLowerCase()
	}
	
	@Override
	public String getRequest(){
		if (mockResult != null) {
			return mockResult.getMockRequest().getRequestContent()
		}
		else {
			return null
		}
	}
	
	@Override
	public byte[] getRawResponseData(){
		return mockResult.getRawResponseData()
	}
	
	@Override
	public String getTestCaseLabel(){
		return ""
	}
	
	@Override
	public String getSuitLabel(){
		return ""
	}
	
	@Override
	public String getProjectLabel(){
		return context.getMockService().getProject().getName()
	}
		
    @Override
    public String getMessage() {
	    if (mockResult != null) {
		    return mockResult.getResponseContent()
	    } 
	    else {
		    return null
	    }
    }
	
    @Override
    public StringToStringsMap getRequestHeaders() {
	    if (mockResult != null) {
		    return mockResult.getMockRequest().getRequestHeaders()
	    } 
	    else {
		    return null
	    }
    }

	@Override
	StringToStringsMap getResponseHeaders() {
		return mockResult.getResponseHeaders()
	}

	@Override
    public String getProperty(String property) {
	    return context.getProperty(property)
    }

    @Override
    public String getPropertyRecursive(String property) {
	    def ret = getProperty(property)
	    if (ret == null){
		    MockService ms = context.getMockService()
		    ret = ms.getPropertyValue(property)
		    if (ret == null){
			    ms.getProject().getPropertyValue(property)
			    if (ret==null) log.warn "could not retrieve property: $property"
		    }
	    }
	    return ret
    }

    @Override
    public Map<String, String> getTestCaseProperties() {
	    return [:]
    }

    @Override
    public Map<String, String> getProjectProperties() {
	    def ret = [:]
	    Project p = context.getMockService().getProject()
	    p.getProperties().each { key, prop ->
		    ret[key] = prop.getValue()
	    }
	    return ret
    }

    @Override
    public Map<String, String> getProperties() {
	    def ret = [:]
	    context.getProperties().each { key, prop ->
		    ret[key] = prop.toString()
	    }
	    return ret
    }
	
	@Override
	public Boolean logTestData() {
		if (context.logTestData) {
			return 	context.logTestData.toLowerCase() == "true"
		}
		return false
	}
	
	@Override
	public String logTestDataPath() {
		return context.logTestDataPath
	}
	
	@Override
	public String logTestDataFilesAllowed() {
		return context.logTestDataFilesAllowed
	}

	@Override
	public byte[] getRawRequestData() {
		return mockResult?.getMockRequest()?.getRawRequestData()
	}
	
	
}
