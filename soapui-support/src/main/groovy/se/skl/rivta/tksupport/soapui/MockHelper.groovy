package se.skl.rivta.tksupport.soapui

import org.apache.logging.log4j.*;

import com.eviware.soapui.model.mock.MockOperation;
import com.eviware.soapui.model.mock.MockRequest;
import com.eviware.soapui.model.mock.MockResult;
import com.eviware.soapui.model.mock.MockRunContext;

import groovy.util.slurpersupport.GPathResult
import se.skl.rivta.tksupport.soapui.validation.SchematronValidatorTK

/**
 * Helper class for validating requests in mocks
 */
class MockHelper {
	
	private MockRequest mockRequest
	private MockOperation mockOperation
	private ContextWrapper contextWrapper
	private Logger log
	
	public MockHelper(MockRequest mockRequest, MockOperation mockOperation, log = null) {
		if (this.log == null) { 
			this.log = LogManager.getLogger(this.class)
			this.log.level= Level.WARN
		}
		this.mockRequest = mockRequest
		this.mockOperation = mockOperation
	}
	public MockHelper(MockRunContext context, MockResult mockResult, log = null) {
		if (this.log == null) { 
			this.log = LogManager.getLogger(this.class)
			this.log.level= Level.WARN
		}
		this.contextWrapper = new MockContextWrapper(context, mockResult, this.log)
		this.mockRequest = mockResult.getMockRequest()
		this.mockOperation = mockResult.getMockOperation()
	}
	public MockHelper(ContextWrapper contextWrapper, log = null) {
		if (this.log == null) { 
			this.log = LogManager.getLogger(this.class)
			this.log.level= Level.WARN
		}
		this.contextWrapper = contextWrapper
	}
	
	
	/**
	 * Returns an error message if any empty elements were found in the request.
	 */
	public String getEmptyElementsInRequest() {
		def envelope = new XmlSlurper().parseText(this.mockRequest.requestContent)
		return getEmptyElementsMessage(envelope)
	}
	
	/**
	 * Returns an error message if any schema errors were found in the request.
	 */
	public String getSchemaErrorsInRequest() {
		assert this.mockRequest : "No MockRequest"
		assert this.mockOperation : "No MockOperation"
		
		def wsdlcontext = this.mockOperation.mockService.getMockedInterfaces()[0].getDefinitionContext()
		def msgExchange = new com.eviware.soapui.impl.wsdl.panels.mockoperation.WsdlMockRequestMessageExchange(this.mockRequest, this.mockOperation);
		def schemaValidator = new com.eviware.soapui.impl.wsdl.support.wsdl.WsdlValidator(wsdlcontext);
		def schemaErrors = schemaValidator.assertRequest(msgExchange, false);
	
		return schemaErrors.length > 0 ? "Schema validation failed: " + schemaErrors.collect(){ " \n" + it } : null
	}
	
	/**
	 * Returns an error message if the request does not meet Schematron constraints.
	 *
	 * @param constraintsFile the Schematron constraints file name, relative to project path
	 */
	public String getSchematronErrorsInRequest(String constraintsFileName) {
		def constraintsFile = new File(getProjectPath(), constraintsFileName)
		return getSchematronErrorsInRequest(constraintsFile)
	}
	
	/**
	 * Returns an error message if the request does not meet Schematron constraints.
	 * 
	 * @param constraintsFile the Schematron constraints file as a File object
	 */
	public String getSchematronErrorsInRequest(File constraintsFile) {
		assert this.mockRequest : "No MockRequest"
		
		def validator = new SchematronValidatorTK()
		def schematronErrors = validator.validateMessage(this.mockRequest.requestContent, constraintsFile)
		return schematronErrors.any() ? "Schematron validation failed: " + schematronErrors.collect() { " \n" + it.text + " at " + it.location } : null
	}
	
	/**
	 * ....
	 */
	public void storeRequestResponse() {
		def logger = new RequestResponseLogger(this.contextWrapper)
		logger.storeMockRequestResponse()
	}
	
	private String getEmptyElementsMessage(GPathResult node, String path = "") {
		def message = ""
		path += "/" + node.name()
		node.children().each {
			def value = it.toString()
			def children = it.children().size()
			def attributes = it.attributes().size()
			if (value || children || attributes) {
				message += getEmptyElementsMessage(it, path)
			}
			else {
				message += path + "/" + it.name() + " is empty. \n"
			}
		}
		return message
	}
	
	private String getProjectPath() {
		def projectFilePath = this.mockOperation.mockService.project.path
		int index = projectFilePath.lastIndexOf(File.separator);
		return index == -1 ? "" : projectFilePath.substring(0, index);
	}
}
