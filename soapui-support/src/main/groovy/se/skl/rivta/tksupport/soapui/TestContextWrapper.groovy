package se.skl.rivta.tksupport.soapui

import java.util.Map

import com.eviware.soapui.support.GroovyUtils
import com.eviware.soapui.model.project.Project
import com.eviware.soapui.model.testsuite.TestStepResult
import com.eviware.soapui.model.testsuite.TestCase
import com.eviware.soapui.model.iface.MessageExchange
import com.eviware.soapui.model.testsuite.TestProperty
import com.eviware.soapui.model.testsuite.TestRunContext
import com.eviware.soapui.support.types.StringToObjectMap
import com.eviware.soapui.support.types.StringToStringsMap

class TestContextWrapper implements ContextWrapper {
    
    TestRunContext context
    MessageExchange messageExchange
    private log
    
    public TestContextWrapper(TestRunContext context, MessageExchange messageExchange, log)
    {
	    this.context = context
	    this.messageExchange = messageExchange
        this.log = log
    }

	
	@Override 
	public Long getExchangeTimestamp()
	{
		return messageExchange.getTimestamp()	
	}
	
	@Override
	public Long getTimeTaken() {
		return messageExchange.getTimeTaken()
	}
	@Override
	public String getEndpoint() {
		return messageExchange.getEndpoint()
	}
	
    @Override
    public String getProjectPath() 
    {
        def path = new GroovyUtils(context).getProjectPath()

        if (path != "") {
            return path
        } else {
            return "."
        }
    }

	@Override
	public String getContentType(){
		return messageExchange.getResponse().getContentType().toLowerCase()
	}
	
	@Override
	public String getRequest(){
		if (messageExchange != null) {
			return messageExchange.getRequestContentAsXml()
		}
		else {
			return null
		}
	}
	
	@Override
	public byte[] getRawResponseData(){
		return messageExchange.getRawResponseData()
	}
	
	@Override
	public String getTestCaseLabel(){
		return context.getTestCase().getLabel()
	}
	
	@Override
	public String getSuitLabel(){
		return context.getTestCase().getTestSuite().getLabel()
	}
	
	@Override
	public String getProjectLabel(){
		return context.getTestCase().getTestSuite().getProject().getName()
	}
		
    @Override
    public String getMessage() {
	    if (messageExchange != null) {
		    return messageExchange.getResponseContentAsXml() ?: messageExchange.getResponseContent()
	    } 
	    else {
		    return null
	    }
    }
	
    @Override
    public StringToStringsMap getRequestHeaders() {
	    if (messageExchange != null) {
		    return messageExchange.getRequestHeaders()
	    } 
	    else {
		    return null
	    }
    }

	@Override
	StringToStringsMap getResponseHeaders() {
		return messageExchange.getResponseHeaders()
	}

	@Override
    public String getProperty(String property) {
	    return context.getProperty(property)
    }

    @Override
    public String getPropertyRecursive(String property) {
	    def ret = getProperty(property)
	    if (ret == null){
		    TestCase tc = context.getTestCase()
		    ret = tc.getPropertyValue(property)
		    if (ret == null){
			    tc.getProject().getPropertyValue(property)
			    if (ret==null) log.warn "could not retrieve property: $property"
		    }
	    }
	    return ret
    }

    @Override
    public Map<String, String> getTestCaseProperties() {
	    def ret = [:]
	    TestCase tc = context.getTestCase()
	    tc.getProperties().each { key, prop ->
		    ret[key] = prop.getValue()
	    }
	    return ret
    }

    @Override
    public Map<String, String> getProjectProperties() {
	    def ret = [:]
	    Project p = context.getTestCase().getProject()
	    p.getProperties().each { key, prop ->
		    ret[key] = prop.getValue()
	    }
	    return ret
    }

    @Override
    public Map<String, String> getProperties() {
	    def ret = [:]
	    context.getProperties().each { key, prop ->
		    ret[key] = prop.toString()
	    }
	    return ret
    }
	
	@Override
	public Boolean logTestData() {
		if (context.logTestData) {
			return 	context.logTestData.toLowerCase() == "true"
		}
		return false
	}
	
	@Override
	public String logTestDataPath() {
		return context.logTestDataPath
	}
	
	@Override
	public String logTestDataFilesAllowed() {
		return context.logTestDataFilesAllowed
	}

	@Override
	public byte[] getRawRequestData() {
		return messageExchange?.getRawRequestData()
	}
	
	
}
