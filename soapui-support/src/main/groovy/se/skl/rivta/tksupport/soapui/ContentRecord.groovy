package se.skl.rivta.tksupport.soapui

import com.eviware.soapui.support.XmlHolder
import se.skl.rivta.tksupport.soapui.validation.TimeFilterValidator

class ContentRecord {
	
	private XmlHolder holder

	protected ContentRecord(XmlHolder holder) {
		this.holder = holder
	}
	
	/**
	 * Returns the value of the first element at a specified path in the record.
	 * Will return null if the element is not found.
	 *
	 * @param xPath the path relative to the root element of the record
	 */
	public String firstElementAt(String xPath) {
		def node = holder.getDomNode(xPath)
		return getNodeValue(node)
	}
	
	/**
	 * Returns a list of values for all elements at a specified path in the record.
	 * Will return an empty list if the element is not found.
	 *
	 * @param xPath the path relative to the root element of the record
	 */
	public List<String> allElementsAt(String xPath) {
		def nodes = holder.getDomNodes(xPath)
		if (nodes.size() == 0) {
			return new ArrayList<String>()
		}
		else {
			return nodes.collect { node -> getNodeValue(node) }
		}
	}
	
	/**
	 * Checks whether a timestamp element in the record it is within a specified time range, limits included.
	 * Timestamps may be in format ranging from YYYY to YYYYMMDDhhmmss. When comparing two timestamps with different formats,
	 * the lowest resolution is used for comparison (e.g. when a date and year is compared, only the year part is compared).
	 *
	 * @param xPath the path of the timestamp element relative to the root element of the record
	 * @param lowerLimit start of time range
	 * @param lowerLimit end of time range
	 */
	public boolean timestampInRange(String xPath, String lowerLimit, String upperLimit) {
		def validator = new TimeFilterValidator(this, lowerLimit, upperLimit)
		def errors = validator.validateTimestampInRange(xPath, true)
		return errors.size() == 0
	}

	/**
	 * Checks whether a timestamp element in the record it is before a time limit.
	 * Timestamps may be in format ranging from YYYY to YYYYMMDDhhmmss. When comparing two timestamps with different formats,
	 * the lowest resolution is used for comparison (e.g. when a date and year is compared, only the year part is compared).
	 *
	 * @param xPath the path of the timestamp element relative to the root element of the record
	 * @param lowerLimit start of time range
	 * @param lowerLimit end of time range
	 */
	public boolean timestampIsBefore(String xPath, String limit) {
		def validator = new TimeFilterValidator(this, limit, null)
		def errors = validator.validateTimestampIsBeforeRange(xPath, true)
		return errors.size() == 0
	}

	/**
	 * Checks whether a timestamp element in the record it is after a time limit.
	 * Timestamps may be in format ranging from YYYY to YYYYMMDDhhmmss. When comparing two timestamps with different formats,
	 * the lowest resolution is used for comparison (e.g. when a date and year is compared, only the year part is compared).
	 *
	 * @param xPath the path of the timestamp element relative to the root element of the record
	 * @param lowerLimit start of time range
	 * @param lowerLimit end of time range
	 */
	public boolean timestampIsAfter(String xPath, String limit) {
		def validator = new TimeFilterValidator(this, null, limit)
		def errors = validator.validateTimestampIsAfterRange(xPath, true)
		return errors.size() == 0
	}
	
	private String getNodeValue(node) {
		if(node == null) {
			return null
		}
		def child = node.getFirstChild()
		return child == null ? "" : child.getNodeValue()
	}
}
