package se.skl.rivta.tksupport.soapui

import java.io.File;
import java.util.List;

import com.eviware.soapui.model.iface.MessageExchange
import com.eviware.soapui.model.testsuite.TestRunContext

import groovy.lang.Closure;
import se.skl.rivta.tksupport.soapui.ContextHelperTK;
import se.skl.rivta.tksupport.soapui.validation.DocBookValidator
import se.skl.rivta.tksupport.soapui.validation.FilterOracle
import se.skl.rivta.tksupport.soapui.validation.SchematronValidatorTK
import se.skl.rivta.tksupport.soapui.validation.TimeFilterValidator

class ContentAssertions {
	
	private ContextHelperTK contextHelper

	public ContentAssertions(TestRunContext context, MessageExchange messageExchange, log = null) {
		contextHelper = new ContextHelperTK(context, messageExchange, log)
		contextHelper.logTestData()
	}
	
	public ContentAssertions(ContextWrapper context, log) {
		contextHelper = new ContextHelperTK(context, log)
		contextHelper.logTestData()
	}


	/**
	 * Assert that there is at least one record in a response
	 * 
	 * Example usage:
	 * assertNonEmptyResponse()
	 */
	public void assertNonEmptyResponse() {
		def records = contextHelper.getRecordsList()
		def numberOfRecords = records.size()
		assert numberOfRecords > 0 : "The response had no records"
	}
	
	/**
	 * Assert that there is a specified number of records in a response
	 *
	 * @param expected expected number of records
	 *
	 * Example usage:
	 * Given number of records is 1
	 * assertExactNumberOfRecords(2) will throw assertion error.
	 */
	public void assertExactNumberOfRecords(int expected) {
		def records = contextHelper.getRecordsList()
		def numberOfRecords = records.size()
		assert numberOfRecords == expected : "Incorrect number of records"
	}

	/**
	 * Assert that at least one record in the response has the element with given xPath.
	 *
	 * @param xPath the path to the element within the record
	 *
	 * Example usage:
	 * assertElementExistsInAnyRecord('urn:body/urn:element')
	 */
	public void assertElementExistsInAnyRecord(String xPath) {
		def errors = contextHelper.validateRecordsHaveElement(xPath, false)
		assertNoErrors(errors)
	}

	/**
	 * Assert that all records in the response has the element with given xPath.
	 *
	 * @param xPath the path to the element within the record
	 *
	 * Example usage:
	 * assertElementExistsInAllRecords('urn:body/urn:element')
	 */
	public void assertElementExistsInAllRecords(String xPath) {
		def errors = contextHelper.validateRecordsHaveElement(xPath, true)
		assertNoErrors(errors)
	}
	
	/**
	 * Assert that at least one record in the response does NOT have the element with given xPath.
	 *
	 * @param xPath the path to the element within the record
	 *
	 * Example usage:
	 * assertElementExcludedInAnyRecord('urn:body/urn:element')
	 *
	 */
	public void assertElementExcludedInAnyRecord(String xPath) {
		def errors = contextHelper.validateElementExcludedFromRecords(xPath, false)
		assertNoErrors(errors)
	}
	
	/**
	 * Assert that at all returned records in the response does NOT have the element with given xPath.
	 *
	 * @param xPath the path to the element within the record
	 *
	 * Example usage:
	 * assertElementExcludedInAllRecords('urn:body/urn:element')
	 *
	 */
	public void assertElementExcludedInAllRecords(String xPath) {
		def errors = contextHelper.validateElementExcludedFromRecords(xPath, true)
		assertNoErrors(errors)
	}
	
	/**
	 * Assert that at least one record in the response has a specific value in the element with given xPath.
	 *
	 * @param xPath the path to the element within the record
	 * @param expectedValue the expected element value
	 *
	 * Example usage:
	 * Given urn:body/urn:element == false in all records
	 * assertSpecificValueInAnyRecord('urn:body/urn:element', 'true') will throw assertion error.
	 */
	public void assertSpecificValueInAnyRecord(String xPath, String expectedValue) {
		def errors = contextHelper.validateRecordsHaveElementValue(xPath, [expectedValue], false)
		assertNoErrors(errors)
	}
	
	/**
	 * Assert that all records in the response has a specific value in the element with given xPath.
	 *
	 * @param xPath the path to the element within the record
	 * @param expectedValue the expected element value
	 *
	 * Example usage:
	 * Given urn:body/urn:element == false in any record
	 * assertSpecificValueInAllRecords('urn:body/urn:element', 'true') will throw assertion error.
	 */
	public void assertSpecificValueInAllRecords(String xPath, String expectedValue) {
		def errors = contextHelper.validateRecordsHaveElementValue(xPath, [expectedValue], true)
		assertNoErrors(errors)
	}
	
	/**
	 * Assert that all records in the response has one of the listed valid values in the element with given xPath.
	 *
	 * @param xPath the path to the element within the record
	 * @param validValues the list of valid element values
	 *
	 * Example usage:
	 * Given /urn:body/urn:documentId == '6789' in any record
	 * def expectedDocumentIds = '1234, 1235, 1236'
	 * def idList = expectedDocumentIds.tokenize(',')*.trim()
	 * assertValidValuesInAllRecords('/urn:body/urn:documentId', idList) will throw assertion error.
	 * 
	 */
	public void assertValidValuesInAllRecords(String xPath, List validValues) {
		def errors = contextHelper.validateRecordsHaveElementValue(xPath, validValues, true)
		assertNoErrors(errors)
	}
	
	/**
	 * Assert that for each record where the element exists, its value is not a forbidden value.
	 *
	 * @param xPath the path to the element within the record
	 * @param expectedValue the expected element value
	 *
	 * Example usage:
	 * Given urn:body/urn:element == true in any record
	 * assertNoForbiddenValueInAnyRecord('urn:body/urn:element', 'true') will throw assertion error.
	 *
	 */
	public void assertNoForbiddenValueInAnyRecord(String xPath, String forbiddenValue) {
		def errors = contextHelper.validateValueExcludedFromRecords(xPath, [forbiddenValue])
		assertNoErrors(errors)
	}
	
	/**
	 * Assert that the response matches the constraints in a Schematron file.
	 *
	 * @param constraintsFilePath optional parameter naming the Schematron file
	 *
	 * Example usage:
 	 * assertSchematronConstraints("SomeConstraints.xml")
	 * will throw error if any violation is found in path/file: $projektdir/SomeConstraints.xml
	 */
	public void assertSchematronConstraints(String constraintsFilePath = "constraints.xml") {
		def validator = new SchematronValidatorTK()
		def message = contextHelper.getMessage()
		def path = contextHelper.getFilePath()
		def failures = validator.validateMessage(message, new File(path, constraintsFilePath))
		def errors = failures.collect(new ArrayList<String>()) { it.text + " at " + it.location }
		assertNoErrors(errors)
	}
	
	/**
	 * Assert that the character encoding stated in the http header and xml prolog are allowed and equal (if stated).
	 * Allowed encodings are UTF-8 and UTF-16.
	 *
	 * Example usage:
	 * assertValidEncoding()
	 */
	public void assertValidEncoding() {
		def errors = contextHelper.validateEncoding(["utf-8", "utf-16", ""])
		assertNoErrors(errors)
	}
	
	/**
	 * Assert that for each record where the element exists, at least one has DocBook contents.
	 * Also validates all DocBook content can be properly un-escaped and parsed as xml,
	 * containing only allowed elements. The following elements are allowed by default:
	 * article, info, title, para, section, bibliography
	 *
	 * @param xPath the path to the element
	 * @param List<string> list of allowed elements
	 *
	 * Example usage:
	 * assertDocBookContentInAnyElement('urn:body/urn:element')
	 *
	 */
	public void assertDocBookContentInAnyElement(String xPath, List<String> allowedElements = ['article', 'info', 'title', 'para', 'section', 'bibliography']) {
		def docBookValidator = new DocBookValidator(contextHelper, allowedElements)
		def errors = docBookValidator.validateDocBook(xPath, false)
		assertNoErrors(errors)
	}
	
	/**
	 * Assert that for each record where the element exists, at least one has Base64 contents.
	 * Optionally saves the Base64-decoded data to a file.
	 *
	 * @param xPath the path to the element
	 * @param fileName optionally supply a filename to save the file
	 * @param maxFileSize optional upper limit for file size in bytes (decoded Base64)
	 * @param folder optional folder to store the file (default is ./test-output)
	 *
	 * Example usage:
	 * assertBase64ContentInAnyElement('urn:body/urn:elm')
	 */
	public void assertBase64ContentInAnyElement(String xPath, String fileName = null, int maxFileSize = 0, File folder = null) {
		def errors = contextHelper.validateAndSaveEmbeddedBase64(xPath, fileName, maxFileSize, folder)
		assertNoErrors(errors)
	}
	
	/**
	 * Assert that for each record where a timestamp element exists, it is within a specified time range, limits included.
	 * Timestamps may be in format ranging from YYYY to YYYYMMDDhhmmss. When comparing two timestamps with different formats,
	 * the lowest resolution is used for comparison (e.g. when a date and year is compared, only the year part is compared).
	 *
	 * @param xPath the path to the timestamp element
	 * @param lowerLimit start of time range
	 * @param upperLimit end of time range
	 * @param requireElement require the timestamp element to be present in all records
	 * 
	 * Example usage:
	 * Given value in 'urn:body/urn:elm' == 1995;
	 * assertTimestampInRange('urn:body/urn:elm', '2000', '2010') returns error
	 *
	 */
	public void assertTimestampInRange(String xPath, String lowerLimit, String upperLimit, boolean requireElement = false) {
		def validator = new TimeFilterValidator(contextHelper, lowerLimit, upperLimit)
		def errors = validator.validateTimestampInRange(xPath, requireElement)
		assertNoErrors(errors)
	}
	
	/**
	 * Assert that for each record where a timestamp element exists, it is outside a specified time range.
	 * Timestamps may be in format ranging from YYYY to YYYYMMDDhhmmss. When comparing two timestamps with different formats,
	 * the lowest resolution is used for comparison (e.g. when a date and year is compared, only the year part is compared).
	 *
	 * @param xPath the path to the timestamp element
	 * @param lowerLimit start of time range
	 * @param upperLimit end of time range
	 * @param requireElement require the timestamp element to be present in all records
	 *
	 * Example usage:
	 * Given value in 'urn:body/urn:elm' == 2000 ->
	 * assertTimestampInRange('urn:body/urn:elm', '1995', '2010') will throw assertion error.
	 *
	 */
	public void assertTimestampOutsideRange(String xPath, String lowerLimit, String upperLimit, boolean requireElement = false) {
		def validator = new TimeFilterValidator(contextHelper, lowerLimit, upperLimit)
		def errors = validator.validateTimestampOutsideRange(xPath, requireElement)
		assertNoErrors(errors)
	}
	
	/**
	 * Assert that for each record where a timestamp element exists, it is before the specified limit (and not equal).
	 * Timestamps may be in format ranging from YYYY to YYYYMMDDhhmmss. When comparing two timestamps with different formats,
	 * the lowest resolution is used for comparison (e.g. when a date and year is compared, only the year part is compared).
	 *
	 * @param xPath the path to the timestamp element
	 * @param limit the timestamp limit
	 * @param requireElement require the timestamp element to be present in all records
	 *
	 * Example usage:
	 * Given value in 'urn:body/urn:elm' == 2000 ->
	 * assertTimestampIsBefore('urn:body/urn:elm', '1995') will throw assertion error.
	 *
	 */
	public void assertTimestampIsBefore(String xPath, String limit, boolean requireElement = false) {
		def validator = new TimeFilterValidator(contextHelper, limit, null)
		def errors = validator.validateTimestampIsBeforeRange(xPath, requireElement)
		assertNoErrors(errors)
	}
	
	/**
	 * Assert that for each record where a timestamp element exists, it is after the specified limit (and not equal).
	 * Timestamps may be in format ranging from YYYY to YYYYMMDDhhmmss. When comparing two timestamps with different formats,
	 * the lowest resolution is used for comparison (e.g. when a date and year is compared, only the year part is compared).
	 *
	 * @param xPath the path to the timestamp element
	 * @param limit the timestamp limit
	 * @param requireElement require the timestamp element to be present in all records
	 *
	 * Example usage:
	 * Given value in 'urn:body/urn:elm' == 1995 ->
	 * assertTimestampIsAfter('urn:body/urn:elm', '2000') will throw assertion error.
	 *
	 */
	public void assertTimestampIsAfter(String xPath, String limit, boolean requireElement = false) {
		def validator = new TimeFilterValidator(contextHelper, null, limit)
		def errors = validator.validateTimestampIsAfterRange(xPath, requireElement)
		assertNoErrors(errors)
	}
	
	/**
	 * Assert that for each record where time period elements exists, at least a part of the period is inside a specified 
	 * time range, limits included.
	 * Timestamps may be in format ranging from YYYY to YYYYMMDDhhmmss. When comparing two timestamps with different formats,
	 * the lowest resolution is used for comparison (e.g. when a date and year is compared, only the year part is compared).
	 *
	 * @param xPathStart the path to the time period start element
	 * @param xPathEnd the path to the time period end element
	 * @param lowerLimit start of time range
	 * @param lowerLimit end of time range
	 * @param requireElement require the start element to be present in all records
	 * @param requireElement require the end element to be present in all records
	 *
	 * Example usage:
	 * Given value in 'urn:body/urn:start' == 1995
	 * Given value in 'urn:body/urn:end' == 2000
	 * assertPartOfTimeperiodInRange('urn:body/urn:start', 'urn:body/urn:end', '2010') will throw assertion error.
	 *
	 */
	public void assertPartOfTimeperiodInRange(String xPathStart, String xPathEnd, String lowerLimit, String upperLimit, boolean requireStart = false, boolean requireEnd = false) {
		def validator = new TimeFilterValidator(contextHelper, lowerLimit, upperLimit)
		def errors = validator.validatePartOfTimeperiodInRange(xPathStart, xPathEnd, requireStart, requireEnd)
		assertNoErrors(errors)
	}
	
	/**
	 * Assert that for each record, a constraint expressed as a closure evaluates to true.
	 *
	 * @param recordConstraint a closure with a ContentRecord object as a parameter
	 * @param errorMessage a message to display if the constraint is not met
	 *
	 * Example usage:
	 * Given value in /urn:body/urn:one != 1
	 * Given value in /urn:body/urn:two != 2
	 *
	 * assertForAllRecords({
	 * record -> 
	 * def one = record.firstElementAt('/urn:body/urn:one')
	 * def two = record.firstElementAt('/urn:body/urn:two')
	 * return (one == '1' || two == '2')
	 * }, "I denna post är one inte '1' och two är inte '2'")
	 *
	 *
	 */
	public void assertForAllRecords(Closure recordConstraint, String errorMessage) {
		def errors = contextHelper.validateForAllRecords(recordConstraint, errorMessage)
		assertNoErrors(errors)
	}
	
	/**
	 * Assert that for at least one record, a constraint expressed as a closure evaluates to true.
	 *
	 * @param recordConstraint a closure with a ContentRecord object as a parameter
	 * @param errorMessage a message to display if the constraint is not met 
	 *
	 * Example usage:
	 * Given value in /urn:body/urn:one != 1
	 * Given value in /urn:body/urn:two != 2 
	 *
	 * assertForAnyRecord({
	 * record -> 
	 * def one = record.firstElementAt('/urn:body/urn:one')
	 * def two = record.firstElementAt('/urn:body/urn:two')
	 * return (one == '1' || two == '2')
	 * }, "Hittade ingen post där one är '1' eller two är '2'")
	 *
	 */
	public void assertForAnyRecord(Closure recordConstraint, String errorMessage) {
		def errors = contextHelper.validateForAnyRecord(recordConstraint, errorMessage)
		assertNoErrors(errors)
	}
	
	/**
	 * Assert that for the list of all records, a constraint expressed as a closure evaluates to true.
	 *
	 * @param recordConstraint a closure with a List of ContentRecord objects as a parameter
	 * @param errorMessage a message to display if the constraint is not met
	 *
	 * Example usage:assertForListOfRecords
	 * Given urn:body/urn:something/urn:id == '1234'
	 * Given urn:body/urn:somethingElse/urn:id '5678'
	 *
	 * assertForListOfRecords(
	 *		{ records -> 
	 *			records.size() > records.unique{r -> r.firstElementAt('/urn:id') }.size()
	 * 		}, "Response does not contain multiple Id's")
	 *
	 * Will not throw assertion error because we provided multiple id's
	 *
	 */
	public void assertForListOfRecords(Closure recordConstraint, String errorMessage) {
		def errors = contextHelper.validateForListOfRecords(recordConstraint, errorMessage)
		assertNoErrors(errors)
	}
	
	/**
	 * Assert that for each record where element exists, at least in one of the records the element have a textstring within the given range.
	 *
	 * @param xPath - the path to element
	 * @param lowerLimit - lowest limit of character count
	 * @param upperLimit - highest limit of character count
	 *
	 * Example usage:
	 * Given urn:body/urn:element = 5
	 * assertElementStringLengthInAnyRange('urn:body/urn:element', 1 , 2) will throw assertion error.
	 *
	 *
	 */
	public void assertElementStringLengthInAnyRange(String xPath, Integer lowerLimit, Integer upperLimit) {
		def errors = contextHelper.validateStringLength(xPath, lowerLimit, upperLimit)
		assertNoErrors(errors)
	}
	
	/**
	 * Get a list of ContentRecord objects representing the current current response. This can be used to compose more complex tests.
	 *
	 * Example usage:
	 * context.allRecords = ca.getContentRecords()
	 * if (!context.allRecords) {
	 *   	testRunner.fail("No records")
	 *		return
	 * }
	 * def dates = def startTimes = context.allRecords.collect { record -> record.firstElementAt('/urn:body/urn:start') }
	 * if (!context.dates) {
	 *	testRunner.fail("No data")
	 * } ...
	 */
	public List<ContentRecord> getContentRecords() {
		return contextHelper.getContentRecords()
	}
	
	/**
	 * Assert that a response is correctly filtered according to a specified condition.
	 *
	 * @param allRecords a list of ContentRecord objects representing the unfiltered response
	 * @param filterCondition a Closure with ContentRecord as parameter to determine which records are expected
	 * @param recordDescription a Closure with ContentRecord as parameter to output a string to identify each record (for error messages)
	 * @param filterDescription an optional string to describe the filter parameters used (for error messages)
	 *
	 * Example Usage: 
	 * 
	 * 1) First request 
	 * context.allRecords = getContentRecords
	 *
	 * 2) Second request
	 * def condition = { record ->
	 *	def before = record.timestampIsBefore('urn:body/urn:end', 1995)
	 *	def after = record.timestampIsAfter('urn:body/urn:start', 2000)
	 *	return !before && !after
	 * }
	 *
	 * Given that urn:end timestamp is before 1995 or urn:start is after 2000 in the second response
	 *
	 * assertFilteredResponse(context.allRecords, condition, recordInfo) will throw assertion error.
	 */
	public void assertFilteredResponse(List<ContentRecord> allRecords, Closure filterCondition, Closure recordDescription, String filterDescription = "") {
		def oracle = new FilterOracle(allRecords, filterCondition, recordDescription)
		def errors = oracle.validateFilteredResponse(contextHelper)
		assertNoErrors(errors, filterDescription)
	}
	/**
	 * Assert that an empty list of error messages.
	 * throws assertion error with messages combined incl. prefix.
	 *
	 * @param List<String> of error messages
	 * @param optional messagePrefix
	 *
	 * Usage:
	 * 
	 * def errors = ['Some error message', 'Some other error message']
	 * assertNoErrors(errors)
	 * will throw assertion error "Some error message, Some other error message"
	 *
	 */
	private void assertNoErrors(List<String> errors, String messagePrefix = "") {
		def numberOfErrors = errors.size()
		def message = errors.join(',\n ')
		assert !numberOfErrors : messagePrefix + message
	}
	
}
