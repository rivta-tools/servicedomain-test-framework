package se.skl.rivta.tksupport.soapui

import com.eviware.soapui.model.iface.MessageExchange
import com.eviware.soapui.model.testsuite.TestProperty;
import com.eviware.soapui.support.types.StringToObjectMap
import com.eviware.soapui.support.types.StringToStringsMap

interface ContextWrapper 
{
    String getProjectPath()
    String getMessage()
    String getProperty(String property)
    String getPropertyRecursive(String property)
    Map<String, String> getTestCaseProperties()
    Map<String, String> getProjectProperties()
    Map<String, String> getProperties()
	String getContentType()
	String getRequest()
	String getTestCaseLabel()
	String getProjectLabel()
	Long getExchangeTimestamp()
	Long getTimeTaken()
	String getEndpoint()
	String getSuitLabel()
	StringToStringsMap getRequestHeaders()
	StringToStringsMap getResponseHeaders()
	byte[] getRawResponseData()
	byte[] getRawRequestData()
	Boolean logTestData()
	String logTestDataPath()
	String logTestDataFilesAllowed()
}
