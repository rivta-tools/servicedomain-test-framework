package se.skl.rivta.tksupport.soapui

import com.eviware.soapui.model.mock.MockResult
import com.eviware.soapui.support.types.StringToStringsMap

class RequestResponseLogger {
	
	private ContextWrapper cw

	protected RequestResponseLogger(ContextWrapper cw) {
		this.cw = cw
	}
	
	/**
	 * Store TestCase Request and Response to a file (according to settings in context).
	 */
	protected void storeTestRequestResponse() {
		
		if(!cw.logTestData()) {
			return
		}
		
		def testCaseName = cw.getTestCaseLabel()
		def testSuitName = cw.getSuitLabel()
		def projectName = cw.getProjectLabel()
		
		def logFilePath = new File(cw.logTestDataPath(), projectName + "/" + testSuitName + "/" + testCaseName)
		prepareDirectory(logFilePath)
		
		def logFile = new File(logFilePath, getFormattedTimestamp() + " - " + testCaseName + ".xml ")
		def logText = getXmlProlog()+getTestCaseText()+getRequestResponseText()
		logFile.write(logText, "UTF-8")
	}
	
	/**
	 * Store Mock Request and Response to a file (according to settings in context).
	 */
	protected void storeMockRequestResponse() {
		
		if(!cw.logTestData()) {
			return
		}
		
		def logFilePath = new File(cw.logTestDataPath(), cw.getProjectLabel())
		prepareDirectory(logFilePath)

		def logFile = new File(logFilePath, getFormattedTimestamp() + ".xml")		
		def logText = getXmlProlog()+getRequestResponseWithRecreatedHeadersText()
		logFile.write(logText, "UTF-8")
	}
	
	private String getFormattedTimestamp() {
		return new Date(cw.getExchangeTimestamp()).format("yyyyMMdd-HHmmssSSS")
	}
	
	private String getXmlProlog() {
		return '<?xml version="1.0" encoding="utf-8"?>\n'
	}
	
	private String getTestCaseText() {
		def endpoint = cw.getEndpoint()
		def ExchangeTimeStamp = new Date(cw.getExchangeTimestamp())
		def ExchangeTimeStampFormatted = ExchangeTimeStamp.format("yyyyMMdd-HHmmssSSS")
		def testCaseName = cw.getTestCaseLabel()
		def testCaseProperties = cw.getProjectProperties()
		def timeTaken = cw.getTimeTaken()
		
		return '<!----------------- TEST CASE PROPERTIES ---------- \n \n' +
		       'Endpoint: ' + endpoint + '\n' +
		       'Date: ' + ExchangeTimeStamp + '\n' +
		       'Testcase: ' + testCaseName + ' \n' +
		       'SOAP Message Exchange Time Taken: ' + timeTaken + ' milliseconds \n' +
		       'Testcase properties: ' + testCaseProperties + '--->\n \n'
	}

	private String getRequestResponseText() {
		def requestContentType = cw.getRequestHeaders()?.get('Content-Type')?.get(0)
		return '<!------------------ REQUEST ---------------------->\n' +
		       getRawContentString(requestContentType, cw.getRawRequestData()) +
		       '\n \n<!------------------ RESPONSE --------------------->\n' +
		       getRawContentString(cw.getContentType(), cw.getRawResponseData())
	}
	
	private static String getRawContentString(String contentType, byte[] rawData) {
		if (!rawData) {
			return ""
		}
		def contentTypeMatcher = (contentType =~ /charset=(.+)[$;\s]*/)
		def contentTypeEncoding = contentTypeMatcher.size() ? contentTypeMatcher[0][1] : "UTF-8"
		return new String(rawData, contentTypeEncoding)
	}

	// To be used for logging in mocks, where headers are not included in raw data
	private String getRequestResponseWithRecreatedHeadersText() {

		return '<!--------------- REQUEST HEADERS ----------------->\n' +
				formatHeaders(cw.getRequestHeaders()) + '\n' +
				'<!------------------ REQUEST ---------------------->\n' +
				cw.getRequest() + '\n\n' +
				'<!-------------- RESPONSE HEADERS ----------------->\n' +
				formatHeaders(cw.getResponseHeaders()) + '\n' +
				'<!------------------ RESPONSE --------------------->\n' +
				cw.getMessage()
	}

	private static String formatHeaders(StringToStringsMap headersMap) {
		def headers = ""
		headersMap.each { header ->
			def value = header.value.size() == 1 ? header.value[0] : header.value.toString()
			headers += header.key + ': ' + value + '\n'
		}
		return headers
	}
	
	private void prepareDirectory(File logFilePath) {
		logFilePath.mkdirs()
		def logFiles = logFilePath.listFiles().findAll { it.name ==~ /.*xml/ }
		def numberOfLogFiles = logFiles.size()
		int numberOfLogFilesAllowed = cw.logTestDataFilesAllowed() as Integer
							
		if (numberOfLogFilesAllowed + 1 == numberOfLogFiles) {
			logFiles[0].delete()
		} else if (numberOfLogFiles > numberOfLogFilesAllowed) {
			logFiles[0].delete()
			logFiles[1].delete()
		}
	}
}
