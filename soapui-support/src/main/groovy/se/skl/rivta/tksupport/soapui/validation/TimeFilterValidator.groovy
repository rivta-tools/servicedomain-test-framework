package se.skl.rivta.tksupport.soapui.validation

import java.util.List;

import se.skl.rivta.tksupport.soapui.ContentRecord
import se.skl.rivta.tksupport.soapui.ContextHelperTK

class TimeFilterValidator {
	
	private ContextHelperTK contextHelper
	private ContentRecord contentRecord
	private String lowerLimit, upperLimit
	
	private List<String> timestampPaths
	private List<Object> timePeriodPaths
	
	public TimeFilterValidator(ContextHelperTK contextHelper, String lowerLimit, String upperLimit) {
		this.contextHelper = contextHelper
		this.lowerLimit = convertDate(lowerLimit)
		this.upperLimit = convertDate(upperLimit)
	}
	
	public TimeFilterValidator(ContentRecord contentRecord, String lowerLimit, String upperLimit) {
		this.contentRecord = contentRecord
		this.lowerLimit = convertDate(lowerLimit)
		this.upperLimit = convertDate(upperLimit)
	}
	
	public List<String> validateTimestampInRange(String xPath, boolean requireElement) {
		def fails = new ArrayList<String>()
		def values = getElementValues(xPath)
		if (values == null) {
			return fails
		}
		
		values = convertDates(values)
		
		values.eachWithIndex { value, index ->
			if (requireElement && value == null) {
				fails.add("In record number ${index+1}: Element with path: ${xPath} was missing")
			}
			if (value != null && !insideLowerLimit(value)) {
				fails.add("In record number ${index+1}: Timestamp $value is before limit $lowerLimit")
			}
			if (value != null && !insideUpperLimit(value)) {
				fails.add("In record number ${index+1}: Timestamp $value is after limit $upperLimit")
			}
		}
		return fails
	}
	
	public List<String> validateTimestampOutsideRange(String xPath, boolean requireElement) {
		def fails = new ArrayList<String>()
		def values = getElementValues(xPath)
		if (values == null) {
			return fails
		}
		
		values = convertDates(values)
		
		values.eachWithIndex { value, index ->
			if (requireElement && value == null) {
				fails.add("In record number ${index+1}: Element with path: ${xPath} was missing")
			}
			if (value != null && insideLowerLimit(value) && insideUpperLimit(value)) {
				fails.add("In record number ${index+1}: Timestamp $value is in range from $lowerLimit to $upperLimit")
			}
		}
		return fails
	}
	
	public List<String> validatePartOfTimeperiodInRange(String xPathStart, String xPathEnd, boolean requireStart, boolean requireEnd) {
		def fails = new ArrayList<String>()
		def startValues = getElementValues(xPathStart)
		if (requireStart && !startValues) {
			fails.add("Element with path: ${xPathStart} was missing")
		}
		def endValues = getElementValues(xPathEnd)
		if (requireEnd && !endValues) {
			fails.add("Element with path: ${xPathEnd} was missing")
		}
		[startValues, endValues].transpose().eachWithIndex { values, index ->
			def start = values[0]
			def end = values[1]
			if (requireStart && start == null) {
				fails.add("In record number ${index+1}: Element with path: ${xPathStart} was missing")
			}
			if (requireEnd && end == null) {
				fails.add("In record number ${index+1}: Element with path: ${xPathEnd} was missing")
			}
			if ((!insideLowerLimit(start) && !insideLowerLimit(end)) || (!insideUpperLimit(start) && !insideUpperLimit(end)) ) {
				fails.add("In record number ${index+1}: Time period from  $start to $end is outside range from $lowerLimit to $upperLimit")
			}
		}
		return fails
	}
	
	public List<String> validateTimestampIsBeforeRange(String xPath, boolean requireElement) {
		def fails = new ArrayList<String>()
		def values = getElementValues(xPath)
		if (requireElement && !values) {
			fails.add("Element with path: ${xPath} was missing")
		}
		if (values == null) {
			return fails
		}
		values = convertDates(values)
		
		values.eachWithIndex { value, index ->
			if (requireElement && value == null) {
				fails.add("In record number ${index+1}: Element with path: ${xPath} was missing")
			}
			if (value != null && insideLowerLimit(value)) {
				fails.add("In record number ${index+1}: Timestamp $value is after or equal to $lowerLimit")
			}
		}
		return fails
	}
	
	public List<String> validateTimestampIsAfterRange(String xPath, boolean requireElement) {
		def fails = new ArrayList<String>()
		def values = getElementValues(xPath)
		if (requireElement && !values) {
			fails.add("Element with path: ${xPath} was missing")
		}
		if (values == null) {
			return fails
		}
		values = convertDates(values)
		
		values.eachWithIndex { value, index ->
			if (requireElement && value == null) {
				fails.add("In record number ${index+1}: Element with path: ${xPath} was missing")
			}
			if (value != null && insideUpperLimit(value)) {
				fails.add("In record number ${index+1}: Timestamp $value is before or equal to $upperLimit")
			}
		}
		return fails
	}
	
	private boolean insideLowerLimit(String timestamp) {
		if (timestamp == null || lowerLimit == null) {
			return true
		}
		def resolution = [timestamp.size(), lowerLimit.size()].min()
		return timestamp.take(resolution) >= lowerLimit.take(resolution)
	}
	
	private boolean insideUpperLimit(String timestamp) {
		if (timestamp == null || upperLimit == null) {
			return true
		}
		def resolution = [timestamp.size(), upperLimit.size()].min()
		return timestamp.take(resolution) <= upperLimit.take(resolution)
	}
	private getElementValues(String xPath) {
		if (this.contextHelper) {
			return this.contextHelper.getElementValues(xPath, true)
		}
		if (this.contentRecord) {
			return this.contentRecord.allElementsAt(xPath)
		}
	}

	public List<String> convertDates(List<String> values) {
		
		if (values == null){
			return values
		}

		return values.collect { value -> convertDate(value) }
	}
	
	public String convertDate(String value) {
		if (value != null && value.contains('T')) {
			value = value.replaceAll('T','')
			value = value.replaceAll(':','')
			value = value.replaceAll('-','')
		}
		return value
	}
}
