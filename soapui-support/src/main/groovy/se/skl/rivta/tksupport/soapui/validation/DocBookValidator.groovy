package se.skl.rivta.tksupport.soapui.validation

import java.util.List

import org.apache.logging.log4j.*;

import se.skl.rivta.tksupport.soapui.ContextHelperTK

class DocBookValidator {
	
	private ContextHelperTK contextHelper
	private List<String> allowedElements
	private Logger log = LogManager.getLogger(this.class)

	public DocBookValidator(ContextHelperTK contextHelper, List<String> allowedElements) {
		this.contextHelper = contextHelper
		this.allowedElements = allowedElements
	}
		
	protected List<String> validateDocBook(String xPath, boolean docBookRequiredForAll) {
		def fails = new ArrayList<String>()
		def values = contextHelper.getElementValues(xPath)
		
		def numberOfDocBooks = 0
		
		values.each {
			if (docBookRequiredForAll || it.startsWith('<?xml') || it.startsWith('<article')) {
				try {
					def docBook = new XmlSlurper().parseText(it)
					fails.addAll(validateDocBookElements(docBook))
				} catch(all) {
					fails.add("Failed during DocBook validation: " + all.message)
				}
				numberOfDocBooks++
			}
		}
		
		if (numberOfDocBooks == 0) {
			fails.add("No DocBook found")
		}
		
		return fails
	}
	
	private List<String> validateDocBookElements(docBook) {
		def fails = new ArrayList<String>()
		docBook.children().each { child ->
			def elementName = child.name()
			if (!(elementName in allowedElements)) {
				fails.add("Element $elementName is not allowed")
			}
			fails.addAll(validateDocBookElements(child))
		}
		return fails
	}
	
}
