package se.skl.rivta.tksupport.soapui.validation

import java.util.List;

import se.skl.rivta.tksupport.soapui.ContentRecord;
import se.skl.rivta.tksupport.soapui.ContextHelperTK;

class FilterOracle {
	
	private List<ContentRecord> allRecords
	private Closure filterCondition
	private Closure recordDescription
	
	public FilterOracle(List<ContentRecord> allRecords, Closure filterCondition, Closure recordDescription) {
		this.allRecords = allRecords
		this.filterCondition = filterCondition
		this.recordDescription = recordDescription
	}
	
	public List<String> validateFilteredResponse(ContextHelperTK contextHelper) {
		def actual = contextHelper.getContentRecords().collect(recordDescription)
		def expected = this.allRecords.findAll(this.filterCondition).collect(recordDescription)
		
		def extra = actual.findAll{ !expected.contains(it) }.collect{"Unexpected record: " + it}
		def missing = expected.findAll { !actual.contains(it) }.collect{"Missing record: " + it}
		
		return extra + missing
	}

}
