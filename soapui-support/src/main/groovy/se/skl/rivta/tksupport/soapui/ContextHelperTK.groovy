package se.skl.rivta.tksupport.soapui


import groovy.lang.Closure;

import java.util.List
import java.util.Map
import java.util.Date

import org.apache.logging.log4j.*

import org.w3c.dom.Node
import com.eviware.soapui.support.GroovyUtils
import com.eviware.soapui.support.XmlHolder

import se.skl.rivta.tksupport.soapui.TestContextWrapper

import com.eviware.soapui.model.iface.MessageExchange
import com.eviware.soapui.model.testsuite.TestRunContext
import com.eviware.soapui.model.testsuite.TestRunner
import com.eviware.soapui.model.testsuite.MessageExchangeTestStepResult


/**
 * Helper class used to handle context operations.
 */
class ContextHelperTK {

	private Logger log = LogManager.getLogger(ContextHelperTK.class)
	private String message;
	private TestRunContext context
	private String path
	private TestRunner _TR
	private namespaces = [:]
	private ContextWrapper privTestStepContext
	private Map<String, String> settings = [:]
	private contentType
	private rawData
	private final ENCODING = ["utf-8", "utf-16", ""]
	
	/** 
	 * Project property name for storing the xpath which is used to find each record nodes in a response. 
	 *
	 */
	public static final String RECORD_ELEMENT_PATH =  "RecordElementPath"
	public static final String XMLNS = "xmlns:"
	
	/**
	 * Constructor for ContextHelperTK class
	 * 
	 * @param context current context for test execution
	 * @param messageExchange message package containing parameters
	 * @param log local log that ContextHelperTK.Class will log to incase of failures
	 */
	public ContextHelperTK(TestRunContext context, MessageExchange messageExchange, log = null) {
		try{
			def groovyUtils = new GroovyUtils(context)
			this.path = groovyUtils.projectPath
		} catch (Exception e){}
		
		this.log = LogManager.getLogger(this.class)
		this.log.level= Level.WARN
		
		this.context = context
		_TR = context.getTestRunner()
		this.message = messageExchange.response.getResponseContent()
		privTestStepContext = new TestContextWrapper(context, messageExchange, this.log)
		extractProperties()
	}
	

	public ContextHelperTK(ContextWrapper context, log) {
		if (log==null) {
			log = LogManager.getLogger(this.class)
		}
		this.log = log
		privTestStepContext = context
		if (privTestStepContext != null) {
			extractProperties()
		}
	}
	
	protected String getFilePath() {
		return this.path
	}
	
	protected String getMessage() {
		return this.message
	}
	
	/**
	 * Method used to fail current test.
	 * When test-running scripts in SoapUI, the TestRunner instance is null. 
	 * This method will issue a warning log instead of breaking the script.
	 * 
	 * @param message Reason for failing
	 */
	public void fail(String message) {
		if (elementNotNull(_TR)) {
			_TR.fail(message)
		} else {
			log.warn("Failing test: $message")
		}
	}
	
	/**
	 * Method used to trigger logging of the request and response of the current data.
	 * 
	 * Logging is configurable in data.xml:	
	 *		<globaldata>
	 *		    <logTestData>true</logTestData>
	 *		    <logTestDataPath>C:/temp/SOAP-UI/</logTestDataPath>
	 *		    <logTestDataFilesAllowed>5</logTestDataFilesAllowed>
	 *		</globaldata>
	 */
	public void logTestData() {
		def requestResponseLogger = new RequestResponseLogger(privTestStepContext)
		requestResponseLogger.storeTestRequestResponse()
	}

	protected XmlHolder getXmlHolder(Node node = null) {
		def msg
		XmlHolder ret
		if (!elementNotNull(node)) {
			msg = privTestStepContext.getMessage()
			if (elementNotNull(msg)) {
				ret = new XmlHolder(msg)
			}
		} else {
			ret = new XmlHolder(node)
		}
		namespaces.each { ns -> ret.declareNamespace(ns.key, ns.value) }
		
		return ret
	}
	
	/**
	 * General tool to check each record node for matching values in given xPath
	 *
	 * @param xPath the path to the value
	 * @param expected a list of expected values
	 * @return a list of failed searches.
	 */
	protected List<String> validateRecordsHaveElementValue(String xPath, List expected, boolean requiredForAll = true) {
		def nodeName = {Node n-> if (n.localName == null) n.parentNode.localName else n.localName }
		List<String> fails = new ArrayList<String>()
		def rl = recordsList
		if (rl.size() == 0) {
			return ["No records found"]
		}
		rl.eachWithIndex { recordNode, index ->
			def holder = getXmlHolder(recordNode)
			def nodes = holder.getDomNodes(xPath)
			
			if (nodes.size() == 0) {
				fails.add("In ${getRecordElementPath()} record number ${index+1}: Element with path: ${xPath} was missing")
			} 			
			else {
				def nodeValues = nodes.collect { node -> elementNotNull(node) ? node.getFirstChild().getNodeValue() : "" }
				if ( !nodeValues.any { value -> value in expected } ) {
					fails.add("In ${getRecordElementPath()} record number ${index+1}:  No element had matching value at path ${xPath} (values: ${nodeValues}, expected: ${expected})")
				}
			}		
		}
		if (!requiredForAll) {
			return fails.size() < rl.size() ? new ArrayList<String>() : ["No records had matching value at path ${xPath} (expected: ${expected})"]
		}
		return fails
	}

	protected List<String> validateElementExcludedFromRecords(String xPath,  boolean requiredForAll = false) {
		def nodeName = {Node n-> if (n.localName == null) n.parentNode.localName else n.localName }
		List<String> fails = new ArrayList<String>()
		def rl = recordsList
		if (rl.size() == 0) {
			return ["No records found"]
		}
		rl.eachWithIndex { recordNode, index ->
			def holder = getXmlHolder(recordNode)
			Node node = holder.getDomNode(xPath)
			if (elementNotNull(node)){
				fails.add("In ${getRecordElementPath()} record number ${index+1}: Expected element at path ${xPath} to be excluded but was found")
			} 
		}
		if (!requiredForAll) {
			return fails.size() < rl.size() ? new ArrayList<String>() : ["Expected element at path ${xPath} to be excluded from at least one record but was found in all"]
		}
		return fails
	}
	
	protected List<String> validateValueExcludedFromRecords(String xPath,  List forbiddenValues) {
		def nodeName = {Node n-> if (n.localName == null) n.parentNode.localName else n.localName }
		List<String> fails = new ArrayList<String>()
		def rl = recordsList
		if (rl.size() == 0) {
			return ["No records found"]
		}
		rl.eachWithIndex { recordNode, index ->
			def holder = getXmlHolder(recordNode)
			def nodes = holder.getDomNodes(xPath)
			if (nodes.any()) {
				def foundForbidden = nodes.collect { node -> elementNotNull(node) && node.getFirstChild().getNodeValue() in forbiddenValues }
				if ( foundForbidden.any() ) {
					fails.add("In ${getRecordElementPath()} record number ${index+1}: Forbidden value(s) ${foundForbidden} found in element at path ${xPath}")
				}
			}
		}
		return fails
	}
		
	protected List<String> validateEncoding(List allowedEncodings) {
		List<String> fails = new ArrayList<String>()
		
		// Check encoding set in content-type
		def contentType = privTestStepContext.getContentType() ?: ""
		log.debug "Content-Type: " + contentType
		
		def contentTypeMatcher = (contentType =~ /charset=(.+)[$;\s]*/)
		def contentTypeEncoding = contentTypeMatcher.size() ? contentTypeMatcher[0][1] : ""
		contentTypeEncoding = contentTypeEncoding.toLowerCase()
		if(!(contentTypeEncoding in allowedEncodings)){
			fails.add("Invalid Content-Type: ${contentType}")
		}
		
		// Check encoding set in xml prolog
		def rawMessage = privTestStepContext.getRawResponseData() ?: new byte[0]
		def message = contentTypeEncoding ? new String(rawMessage, contentTypeEncoding)
										  : new String(rawMessage)

		def prologMatcher = (message =~ /<\?xml.*\?>/)
		def prolog = prologMatcher.size() ? prologMatcher[0] : ""

		log.debug "XML Prolog: " + prolog

		def encodingMatcher = (prolog =~ /encoding=["'](.+)["']/)
		def prologEncoding = encodingMatcher.size() ? encodingMatcher[0][1] : ""
		prologEncoding = prologEncoding.toLowerCase()
		
		if(!(prologEncoding in allowedEncodings)) {
			fails.add("Invalid XML Prolog: ${prolog}")
		}
		
		// Check that encodings equal if stated
		if(contentTypeEncoding && prologEncoding && (contentTypeEncoding != prologEncoding)) {
			fails.add("Encoding mismatch between Content-Type [${contentType}] and XML Prolog [${prolog}]")
		}
		
		return fails
	}
	/**
	 * General tool to check each record node for the existence of a given xPath
	 *
	 * @param xPath the path to the value
	 * @return a list of failed searches.
	 */
	protected List<String> validateRecordsHaveElement(String xPath, boolean requiredForAll = false) {
		List<String> fails = new ArrayList<String>()
		def rl = recordsList
		if (rl.size() == 0) {
			return ["No records found"]
		}
		rl.eachWithIndex { recordNode, index ->
			def holder = getXmlHolder(recordNode)
			Node node = holder.getDomNode(xPath)
			if (!elementNotNull(node)) {
				fails.add("In ${getRecordElementPath()} record number ${index+1}: Element with path: ${xPath} was missing")
			} else if (node.getFirstChild() == null) {
				fails.add("In ${getRecordElementPath()} record number ${index+1}: Element with path: ${xPath} was empty")
			}
		}
		if (requiredForAll == false && rl.size() > 1) {
			return fails.size() < rl.size() ? new ArrayList<String>() : ["No records had an element at path ${xPath}"]
		}
		return fails
	}
	
	protected List<String> validateAndSaveEmbeddedBase64(String xPath, String fileName, int maxFileSize, File folder) {
		def fails = new ArrayList<String>()
		
		def values = getElementValues(xPath)
		if (values.size() == 0) {
			fails.add("Embedded Base64 content not found")
			return fails
		}
		
		def bytes = values[0].decodeBase64()
		def fileSize = bytes.length
		if (maxFileSize && fileSize > maxFileSize) {
			fails.add("File size $fileSize is larger than limit $maxFileSize bytes")
		}
		
		def outputFilePath = getOutputFilePath(fileName, folder)
		if (outputFilePath != null) {
			FileOutputStream stream = new FileOutputStream(outputFilePath);
			try {
				stream.write(bytes)
			} catch (all)	{
				fails.add("Could not write file $outputFilePath")
			} finally {
				stream.close();
			}
		}

		return fails
	}
	
	/**
	 * Get values for the specified element from all records.
	 * @param addNullIfExcluded if true, missing elements will have null entries in result (otherwise omitted)
	 */
	protected List<String> getElementValues(String xPath, boolean addNullIfExcluded = false) {
		def values = new ArrayList<String>()
		def rl = recordsList
		rl.each { recordNode ->
			def holder = getXmlHolder(recordNode)
			Node node = holder.getDomNode(xPath)
			if (elementNotNull(node) && node.getFirstChild().getNodeValue() != null) {
				values.add(node.getFirstChild().getNodeValue())
			}
			else if (addNullIfExcluded) {
				values.add(null)
			}
		}
		return values
	}
	
	protected List<String> validateForAllRecords(Closure recordConstraint, String errorMessage) {
		def fails = new ArrayList<String>()
		def records = getContentRecords()
		if (records.size() == 0) {
			return ["No records found"]
		}
		records.eachWithIndex { record, index ->
			try {
				assert recordConstraint(record) : errorMessage
			}
			catch (AssertionError e){
				fails.add("In ${getRecordElementPath()} record number ${index+1}: ${e.getMessage()}")
			}
		}
		return fails
	}
	
	protected List<String> validateForAnyRecord(Closure recordConstraint, String errorMessage) {
		def records = getContentRecords()
		if (records.size() == 0) {
			return ["No records found"]
		}
		def fails = validateForAllRecords(recordConstraint, errorMessage)

		return fails.size() < records.size() ? new ArrayList<String>() : [errorMessage]
	}
	
	protected List<String> validateForListOfRecords(Closure recordConstraint, String errorMessage) {
		def fails = new ArrayList<String>()
		def records = getContentRecords()
		if (!recordConstraint(records)) {
			fails.add(errorMessage)
		}
		return fails
	}
	
	protected List<String> validateStringLength(xPath, lowerLimit, upperLimit, boolean requiredForAll = false) {
		List<String> fails = new ArrayList<String>()
		def rl = recordsList
		if (rl.size() == 0) {
			return ["No records found"]
		}
		
		def low = lowerLimit as Integer
		def high = upperLimit as Integer
		
		rl.eachWithIndex { recordNode, index ->
			def holder = getXmlHolder(recordNode)
			Node node = holder.getDomNode(xPath)
			
			if (!elementNotNull(node))	{
				fails.add("In ${getRecordElementPath()} record number ${index+1}: Element with path: ${xPath} was missing")
			} else if (node.getFirstChild() == null) {
				fails.add("In ${getRecordElementPath()} record number ${index+1}: Element with path: ${xPath} was empty")
			} else {
			
			def element = node.getFirstChild().getNodeValue() as String
			def StringLength = element.length() as Integer
			
				if (StringLength < low || StringLength > high) {
					fails.add("In ${getRecordElementPath()} record number ${index+1}: Element with path: ${xPath} is not within the given character count range. String length is " + StringLength)
				}
			}
		}
		if (requiredForAll == false && rl.size() > 1) {
			return fails.size() < rl.size() ? new ArrayList<String>() : ["No records had an element with a textstring within the given range at path ${xPath}"]
		}
		return fails
		
	}
	
	protected List<ContentRecord> getContentRecords() {
		return getRecordsList().collect { new ContentRecord(getXmlHolder(it)) }
	}
	
	protected Node[] getRecordsList() {
		XmlHolder h = getXmlHolder()
		def domNodes = h.getDomNodes(getRecordElementPath())
		if (!domNodes.any()) {
			checkForNamespaceIssues()
		}
		return domNodes
	}

	private boolean elementNotNull(node){
		return (node != null ) ? true : false
	}
	
	private File getOutputFilePath(String fileName, File folder) {
		def outputFilePath = null
		if (fileName != null) {
			def outputFileFolder = folder ?: new File(this.path, 'test-output')
			outputFileFolder.mkdirs()
			outputFilePath = new File(outputFileFolder, fileName)
		}
		return outputFilePath
	}
	
	
	
	/**
	 * Internal method - not for public use -
	 */
	private getRecordElementPath() {
		def path = settings[RECORD_ELEMENT_PATH]
		assert path : "No record definition. Enter an xPath in project property ${RECORD_ELEMENT_PATH}"
		return path
	}
	
	/**
	 * Internal method - not for public use -
	 */
	private extractProperties() {
		[privTestStepContext.getProjectProperties(),
		 privTestStepContext.getTestCaseProperties(),
		 privTestStepContext.getProperties()].each {
			it.each { key, val ->
				if(key.startsWith(XMLNS)) {
					declareNamespace(key.substring(XMLNS.size()), val)
				}
				 else {
					settings[key] = val
				}
			}
		}
	}
	
	private checkForNamespaceIssues() {
		if (!namespaces.any()) {
			log.warn("There are no XML namespaces defined in the project.")
		}
		namespaces.each { alias, namespace ->
			if (message != null && !message.contains(namespace)) {
				log.warn("Message does not include namespace " + namespace)
			}
		}
	}
	
	/**
	 * Internal method - not for public use -
	 */
	private declareNamespace(alias, namespace) {
		namespaces[alias] = namespace
	}
	
	/**
	 * Internal element - not for public use - 
	 */
	def static setRecordElementPath(String xPath, project)
	{
		project.setPropertyValue(this.RECORD_ELEMENT_PATH, xPath)
	}
	
	/**
	 * Internal element - not for public use - 
	 */
	def static setXmlNamespaces(namespaces, project)
	{
		namespaces.each { alias, namespace ->
			project.setPropertyValue(this.XMLNS + alias, namespace)
		}
	}
	
}