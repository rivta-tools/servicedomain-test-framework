#!/usr/bin/env groovy

def image

try {
    node {
        stage('Checkout sources') {
            dir('framework') {
                checkout scm
            }
        }
        stage('Build soapui-support') {
            def gradle = docker.image('gradle:4.0-jdk8')
            gradle.inside("-e GRADLE_USER_HOME=$WORKSPACE") {
                sh 'cd framework/soapui-support && gradle clean fetchJars build groovydoc'
                junit 'framework/soapui-support/build/test-results/test/*.xml'
                archiveArtifacts 'framework/soapui-support/build/libs/*.jar'
            }
        }
        stage('Build docker image') {
            image = docker.build('rivta-tools/soapui', 'framework')
        }
        stage('Domain tests') {
            dir('domain') {
              deleteDir()
              sh 'git clone https://bitbucket.org/rivta-domains/riv.clinicalprocess.activityprescription.actoutcome .'
            }
            image.inside("-e JAVA_OPTS=-Duser.home=$WORKSPACE") {
                try {
                    sh "cd domain/test-suite && selftest_all_testsuites.sh /opt/soapui/bin/ext"
                    sh "cd domain/test-suite && verify_all_testsuites.sh"
                    sh "cd domain/test-suite && update_all_testdocs.sh"
                }
                finally {
                    junit 'domain/test-suite/**/log/*.xml'
                    if (currentBuild.result == 'UNSTABLE') {
                        currentBuild.result = 'FAILURE'
                    }
                }
            }
        }
        stage('Publish soapui-support') {
            sshagent(['jenkinsineratk-bitbucket']) {
                dir('publish') {
                    deleteDir()
                    sh 'git clone ssh://git@altssh.bitbucket.org:443/rivta-tools/rivta-tools.bitbucket.org.git .'
                }
                dir('publish/soapui-support') {
                    sh 'cp -r $WORKSPACE/framework/soapui-support/build/docs/groovydoc/* groovydoc'
                    sh 'cp $WORKSPACE/framework/soapui-support/build/libs/*.jar builds'
                    sh 'cd builds && ./create-index.sh > index.html'
                    sh "git add ."
                    sh "git commit -m 'Publishing build artifacts (automatic push from Jenkins)' || echo 'Commit returned 1. There is probably nothing to commit.'"
                    sh "git push origin master"
                }
            }
        }
        stage('Publish docker image') {
            docker.withRegistry('https://docker.drift.inera.se', 'd9dd45c5-894d-4a8f-8316-ae3794fc96b9') {
                image.push('latest')
            }
        }
    }
}
catch (e) {
    currentBuild.result = "FAILED"
    throw e
}
finally {
    notifyBuild(currentBuild.result)
}

def notifyBuild(buildStatus) {
    if (buildStatus) {
        def subject = "${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'"
        def details = "${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]': \nCheck console output at ${env.BUILD_URL}"
        emailext (
            body: details,
            recipientProviders: [[$class: 'DevelopersRecipientProvider']],
            subject: subject
        )
    }
}
