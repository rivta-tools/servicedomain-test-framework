#! /bin/bash
#! verifheaderconsumerhsaid.sh
#! Kontrollerar att header "x-rivta-original-serviceconsumer-hsaid" value="${httpHeaderHsaId}" ska finnas
exitcode=1
STRING="--- Verifying the request header for serviceconsumer-hsaid is set in testcases. ---" 
echo $STRING 
python ../GetScripts/ci-support/verifyheaderconsumerhsaid.py $@
exitcode=$?
exit $exitcode