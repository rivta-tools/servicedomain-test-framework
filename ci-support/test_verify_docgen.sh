#!/bin/bash

# Avbryt vid fel (propagera felkod)
set -e

# startar mockservicen
/bin/bash ../GetScripts/ci-support/mockservicerunner.sh test-suite test-suite/${CONTRACT_NAME}Mock/${CONTRACT_NAME}Mock-soapui-project.xml &

# vänta lite så att mockservicen hinner starta ordentligt
/bin/bash ../GetScripts/ci-support/sleep.sh

# kör igenom alla testfall som finns i SoapUI-projektet
/bin/bash ../GetScripts/ci-support/testrunner.sh test-suite test-suite/${CONTRACT_NAME}/${CONTRACT_NAME}-soapui-project.xml -r -a -flog -j -PIsBuildServer=true

# verifierar att alla testfall i data.xml används av SoapUI-projektet
/bin/bash ../GetScripts/ci-support/verifydataxmlallused.sh test-suite/${CONTRACT_NAME}/data.xml test-suite/${CONTRACT_NAME}/${CONTRACT_NAME}-soapui-project.xml

# verifierar att alla testfall i data.xml är i nummerordning
/bin/bash ../GetScripts/ci-support/verifydataxmlnumberingorder.sh test-suite/${CONTRACT_NAME}/data.xml

# verifierar att alla testfall i SoapUI-projektet har headern "x-rivta-original-serviceconsumer-hsaid" satt till ett värde
/bin/bash ../GetScripts/ci-support/verifyheaderconsumerhsaid.sh test-suite/${CONTRACT_NAME}/${CONTRACT_NAME}-soapui-project.xml

# veriferar att inget testfall i SoapUI-projektet har ett anrop där logisk adressat är satt till "?"
/bin/bash ../GetScripts/ci-support/verifylogicaladdress.sh test-suite/${CONTRACT_NAME}/${CONTRACT_NAME}-soapui-project.xml

# verifierar att alla mock-responser i SoapUI-mock-projektet används av ett testfall
/bin/bash ../GetScripts/ci-support/verifymockresponseallused.sh test-suite/${CONTRACT_NAME}Mock/${CONTRACT_NAME}Mock-soapui-project.xml test-suite/${CONTRACT_NAME}/${CONTRACT_NAME}-soapui-project.xml

# verifierar att alla personnummer som används finns med i en lista med personnummer som är godkända att använda i test
/bin/bash ../GetScripts/ci-support/verifypersonalidentitynumberinlist.sh test-suite/${CONTRACT_NAME}/data.xml ../GetScripts/ci-support/validtestpersonalidentitynumbers.csv

# verifierar att alla personnummer som anges är korrekta enligt Luhn-algoritmen, samt att de har korrekt slutsiffra
/bin/bash ../GetScripts/ci-support/verifypersonalidentitynumberluhn.sh test-suite/${CONTRACT_NAME}/data.xml

# verifierar att alla testfall i SoapUI-projektet är i nummerordning
/bin/bash ../GetScripts/ci-support/verifysoapuixmlnumberingorder.sh test-suite/${CONTRACT_NAME}/${CONTRACT_NAME}-soapui-project.xml

git pull origin ${BRANCH}

# generera ny dokumentation utifrån innehållet i data.xml samt tk-doc_sv.xsl
xsltproc test-suite/${CONTRACT_NAME}/data.xml -o test-suite/${CONTRACT_NAME}/${CONTRACT_NAME}-doc.html
git add test-suite/${CONTRACT_NAME}/*.html
git commit -m 'Updating documentation (automatic push from Jenkins)' || echo 'Commit returned 1. There is probably nothing to commit.'
