#!/bin/bash

CI_SUPPORT=$(dirname $0)
SOAPUI_EXT_LIB=${1:-$PWD}
ERRORS=0

for CONTRACT_NAME in $(find * -type d -prune); do
    TEST_PROJECT="${CONTRACT_NAME}/${CONTRACT_NAME}-soapui-project.xml"
    MOCK_PROJECT="${CONTRACT_NAME}Mock/${CONTRACT_NAME}Mock-soapui-project.xml"
    if [ ! -f ${TEST_PROJECT} ] || [ ! -f ${MOCK_PROJECT} ] ; then
      continue
    fi

    echo "=== Running tests in ${CONTRACT_NAME} with mock service ${CONTRACT_NAME}Mock ==="

    # startar mockservicen
    /bin/bash $CI_SUPPORT/mockservicerunner.sh $SOAPUI_EXT_LIB $MOCK_PROJECT &

    # vänta lite så att mockservicen hinner starta ordentligt
    /bin/bash $CI_SUPPORT/sleep.sh

    # kör igenom alla testfall som finns i SoapUI-projektet
    /bin/bash $CI_SUPPORT/testrunner.sh $SOAPUI_EXT_LIB $TEST_PROJECT -r -a -flog -j -PIsBuildServer=true
    ERRORS=$(($ERRORS+$?))

    # stoppa mockservicen
    pkill -f java
done

exit $ERRORS
