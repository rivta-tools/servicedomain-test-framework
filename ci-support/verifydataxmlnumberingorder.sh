#!/bin/bash

echo "--- Verifying the ordering of test cases in $(basename $1). ---"

testcases=($(grep -oP '(?<=<testcase id=").*?(?=">)' "$1"))

source $(dirname $0)/verifynumberingorder.sh