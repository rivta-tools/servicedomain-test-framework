#!/usr/bin/env python
# -*- coding: utf-8 -*-
# CHECK Header in request. For a request with con:setting id = $self.checkId the $matchHeader is required
#
# Verified for requests in SoapUI 5.2.1                                                  12dec 2016
# Implemented in Jenkins 2.26                                                            20dec 2016
# Condition added for request con:setting id                                             29dec 2016
import xml.sax
import codecs
import sys
import argparse

class CheckSchemaForConsumerIdInHeader(xml.sax.ContentHandler):
    def __init__ (self):
        self.content = ""
        self.testCaseName = None
        self.testStepName = None
        self.inDoc = False
        self.id = None
        self.errors = []
        self.checkId =  "com.eviware.soapui.impl.wsdl.WsdlRequest@request-headers"
             
    def endElement (self, name):
        name = name.encode("utf-8")
        
        if name == "con:testCase" :
            self.testCaseName = None
        elif name == "con:testStep" :
            self.testStepName = None
        elif name  == "con:setting" and self.testStepName:
            self.inDoc = False
            matchHeader = "x-rivta-original-serviceconsumer-hsaid"
            if matchHeader not in self.content and self.id in self.checkId : 
                self.errors.append("TF {}. Header '{}' is missing in request at test step {}." \
                                   .format(self.testCaseName.encode("utf-8"), matchHeader, self.testStepName.encode("utf-8")))
                # Avmarkera rader med # nedan för att köra utskrift/fulständig logg för alla testfall oavsett resultat
                #print "### ERROR : TF {}. Header '{}' is missing in request at test step {}." \
                #                   .format(self.testCaseName.encode("utf-8"), matchHeader, self.testStepName.encode("utf-8"))
            #else :
                #print "TF {}. Header '{}' is found in request at test step {}." \
                #                   .format(self.testCaseName.encode("utf-8"), matchHeader, self.testStepName.encode("utf-8"))
            self.content = ""
            
    def startElement (self, name, attrs):
        name = name.encode("utf-8")
        if name == "con:testCase" :
            self.testCaseName = attrs["name"]
            
        elif name == "con:testStep" and attrs["type"]=="request" :
            self.testStepName = attrs["name"]
            
        elif self.testStepName and name == "con:setting" :
            self.id = attrs["id"] 
            #print "### startELEMENT id = "+self.id 
            self.inDoc = True

    def characters (self, content):
        if self.inDoc:
            self.content += content
       
def main(filnam):
    result = 1;
    csfih = CheckSchemaForConsumerIdInHeader()
    parser = xml.sax.make_parser()
    parser.setContentHandler(csfih)
    try :
        f = open(filnam, "r")
    except Exception as e:
        print e
        return result
    parser.parse(f)
    if csfih.errors :
        for err in csfih.errors:
            print "ERROR: {msg}".format(msg=err)
    else :
        result = 0
        print "OK: Header x-rivta-original-serviceconsumer-hsaid found in all expected requests."
    return result
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Verifying xmlfile headers')
    parser.add_argument("xmlfile")
    args = parser.parse_args()
    exit (main(args.xmlfile))
    

