#!/bin/bash

echo "--- Verifierar att projektfilen inte innehåller ett anrop med logicalAddress = '?'. Kontrollen bortser från de anrop som skapats av SoapUI."

num_of_laq=($(grep -P -A15 'LogicalAddress>\?' "$1" | grep '${patient.*}'))

exitcode=0

n=${#num_of_laq[@]}

if [ "$n" -gt 0 ]; then 
	echo "ERROR: Found a request with logicalAddress = '?'"
	exitcode=1
fi

exit $exitcode

 