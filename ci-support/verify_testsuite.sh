#!/bin/bash

# Avbryt vid fel (propagera felkod)
set -e

CI_SUPPORT=$(dirname $0)
TEST_PROJECT=$1
DATA_FILE="$(dirname $1)/data.xml"

echo "=== Running test-suite verification scripts for ${TEST_PROJECT} ==="

# verifierar att alla testfall i SoapUI-projektet har headern "x-rivta-original-serviceconsumer-hsaid" satt till ett värde
echo "--- Verifying the request header for serviceconsumer-hsaid is set in testcases. ---"
python ${CI_SUPPORT}/verifyheaderconsumerhsaid.py ${TEST_PROJECT}

# veriferar att inget testfall i SoapUI-projektet har ett anrop där logisk adressat är satt till "?"
/bin/bash ${CI_SUPPORT}/verifylogicaladdress.sh ${TEST_PROJECT}

# verifierar att alla testfall i SoapUI-projektet är i nummerordning
/bin/bash ${CI_SUPPORT}/verifysoapuixmlnumberingorder.sh ${TEST_PROJECT}

# verifierar att alla testfall i SoapUI-projektet har standard-assertions
echo "--- Verifying assertions in testcases. ---"
python ${CI_SUPPORT}/verifyassertions.py ${TEST_PROJECT}

if [ ! -f "${DATA_FILE}" ] ; then
  echo "WARNING: No data file at ${DATA_FILE}"
  exit 0
fi

# verifierar att alla testfall i data.xml används av SoapUI-projektet
/bin/bash ${CI_SUPPORT}/verifydataxmlallused.sh ${DATA_FILE} ${TEST_PROJECT}

# verifierar att alla testfall i data.xml är i nummerordning
/bin/bash ${CI_SUPPORT}/verifydataxmlnumberingorder.sh ${DATA_FILE}

# verifierar att alla personnummer som används finns med i en lista med personnummer som är godkända att använda i test
/bin/bash ${CI_SUPPORT}/verifypersonalidentitynumberinlist.sh ${DATA_FILE} ${CI_SUPPORT}/validtestpersonalidentitynumbers.csv

# verifierar att alla personnummer som anges är korrekta enligt Luhn-algoritmen, samt att de har korrekt slutsiffra
/bin/bash ${CI_SUPPORT}/verifypersonalidentitynumberluhn.sh ${DATA_FILE}
