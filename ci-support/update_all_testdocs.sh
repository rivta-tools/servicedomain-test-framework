#!/bin/bash

# Recursively find test-suite/ServiceContractName/data.xml files
# and generate corresponding ServiceContractName-doc.html files.

# Usage: generate.sh [test-suite directory]
DIR=${1:-$PWD}

# Exit on error
set -e

find $DIR -name 'data.xml' -exec sh -c \
    'cd $(dirname {}) && \
    echo "Processing {}" && \
    xsltproc data.xml -o ${PWD##*/}-doc.html' \;
