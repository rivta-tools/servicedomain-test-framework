numberre='^[0-9]+\.[0-9]?\.?[0-9]?$'
exitcode=0
numberorder=()

for i in ${!testcases[*]}
do
	currentnumber=${testcases[$i]}
	if [[ $currentnumber =~ $numberre ]]; then
		numberorder+=($currentnumber)
	fi
done

actual=${numberorder[*]};
echo "Testcase order: $actual"

# Version sort (-V) gives the wanted order, eg 1.9 -> 1.9.1 -> 1.10
sorted=($(printf '%s\n' "${numberorder[@]}" | sort -V))
expected=${sorted[*]}

if [ "$actual" != "$expected" ]; then
    echo "Expected order: $expected"
		exitcode=1
fi

exit $exitcode
