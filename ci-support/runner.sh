set +e

[ -z $SOAPUI_VERSION ] && SOAPUI_VERSION=5.2.1

SOAPUI_CLASSPATH=/opt/soapui/bin/soapui-$SOAPUI_VERSION.jar:/opt/soapui/lib/*
JFXRTPATH=$(java -cp $SOAPUI_CLASSPATH com.eviware.soapui.tools.JfxrtLocator)
SOAPUI_CLASSPATH=$JFXRTPATH:$SOAPUI_CLASSPATH

JAVA_OPTS="-Xms128m -Xmx1024m -Dsoapui.properties=soapui.properties -Dsoapui.home=/opt/soapui/bin"
JAVA_OPTS="$JAVA_OPTS -Dsoapui.ext.libraries=$1"
JAVA_OPTS="$JAVA_OPTS -Dsoapui.ext.listeners=/opt/soapui/bin/listeners"
JAVA_OPTS="$JAVA_OPTS -Dsoapui.ext.actions=/opt/soapui/bin/actions"