#!/usr/bin/env python
# -*- coding: utf-8 -*-

import xml.sax
import codecs
import sys
import argparse

class CheckForExpectedAssertions(xml.sax.ContentHandler):
    def __init__ (self):
        self.testCaseName = None
        self.testStepName = None
        self.inDoc = False
        self.assertions = []
        self.errors = []
        self.warnings = []
        self.checkId =  "com.eviware.soapui.impl.wsdl.WsdlRequest@request-headers"

    def endElement (self, name):
        name = name.encode("utf-8")

        if name == "con:testCase" :
            self.testCaseName = None

        elif name == "con:testStep" and self.testStepName:
            testStep = "{} - {}".format(self.testCaseName.encode("utf-8"), self.testStepName.encode("utf-8"))
            #print testStep
            #for assertion in self.assertions:
            #    print "{} {}".format(assertion['type'], assertion.get('name', ''))
            if not any("SOAP Response" in a['type'] for a in self.assertions):
                self.errors.append("{} is missing 'SOAP Response' assertion.".format(testStep))
            if not any("SOAP Fault" in a['type'] for a in self.assertions):
                self.errors.append("{} is missing 'SOAP Fault' or 'No SOAP Fault' assertion.".format(testStep))
            if not any("Schema Compliance" in a['type'] for a in self.assertions):
                self.warnings.append("{} is missing 'Schema Compliance' assertion.".format(testStep))
            if not any((a['type'] == "GroovyScriptAssertion" and "schematron" in a['name'].lower()) for a in self.assertions):
                self.warnings.append("{} is missing 'Schematron' script assertion.".format(testStep))

            self.assertions = []
            self.testStepName = None

    def startElement (self, name, attrs):
        name = name.encode("utf-8")
        if name == "con:testCase" :
            self.testCaseName = attrs["name"]

        elif name == "con:testStep" and attrs["type"]=="request" :
            self.testStepName = attrs["name"]

        elif self.testStepName and name == "con:assertion" :
            self.assertions.append(attrs)


def main(filename):
    result = 1;
    cfea = CheckForExpectedAssertions()
    parser = xml.sax.make_parser()
    parser.setContentHandler(cfea)
    try :
        f = open(filename, "r")
    except Exception as e:
        print e
        return result
    parser.parse(f)
    if cfea.errors :
        for err in cfea.errors:
            print "ERROR: {msg}".format(msg=err)
    else :
        result = 0
        if cfea.warnings:
            for warn in cfea.warnings:
                print "WARNING: {msg}".format(msg=warn)
        else:
            print "OK: No errors or warnings"
    return result

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Verifying soapui assertions')
    parser.add_argument("xmlfile")
    args = parser.parse_args()
    exit (main(args.xmlfile))
