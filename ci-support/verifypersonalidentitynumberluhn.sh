#!/bin/bash

echo "--- Verifying that all patientids in $(basename $1) have a valid last integer according to Luhn algorithm. ---"

patientids=($(grep -o -P '(?<=<patientId).*(?=<)' "$1"))
penr_re="^(19|20)?[0-9]{2}((0[0-9])|(10|11|12))(([0-2][0-9])|(3[0-1]))[0-9]{4}$"
exitcode=0

for i in ${!patientids[*]}
do
	patientid=${patientids[$i]}
	patientidfiltered=$(echo $patientid | sed 's/.*>//')
		
	if [[ $patientidfiltered =~ $penr_re ]]; then
		patientidshort=${patientidfiltered:2:10}
		sum=0
		
		for i in $(seq 9 -1 0); do
			digit=${patientidshort:$i:1}
			if [ $(((10-i) % 2)) -eq 0 ]; then
				((digit*=2))
				[ ${#digit} -eq 2 ] && digit=$((${digit:0:1}+${digit:1:1}))
			fi
			((sum+=digit))
		done

		if [ $((sum % 10)) -eq 0 ]; then
			echo "OK: $patientidfiltered is recognized as a valid personal identity number using Luhn algorithm."
		else
			sumcalc=0
			
			for i in $(seq 8 -1 0); do
				digit=${patientidshort:$i:1}
				if [ $(((10-i) % 2)) -eq 0 ]; then
					((digit*=2))
					[ ${#digit} -eq 2 ] && digit=$((${digit:0:1}+${digit:1:1}))
				fi
				((sumcalc+=digit))
			done
			
			calculated=$((10 - sumcalc % 10))
			
			if [ $calculated -eq 10 ]; then
				calculated=0
			fi
			
			echo "ERROR: $patientidfiltered has invalid last character ${patientidshort:9:1}, should be $calculated.";
			exitcode=1
		fi
	fi 
done

exit $exitcode

 