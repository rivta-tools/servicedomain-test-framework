#!/bin/bash

echo "--- Verifying that all patientids in $(basename $1) are included in $(basename $2). ---"

patientids=($(grep -o -P '(?<=<patientId).*(?=<)' "$1"))
penr_re="^(19|20)?[0-9]{2}((0[0-9])|(10|11|12))(([0-2][0-9])|(3[0-1]))[0-9]{4}$"
exitcode=0
validtestpersonalidentitynumberscsv=$(cat "$2")

for i in ${!patientids[*]}
do
	patientid=${patientids[$i]}
	patientidfiltered=$(echo $patientid | sed 's/.*>//')
		
	if [[ $patientidfiltered =~ $penr_re ]]; then
		if [[ $validtestpersonalidentitynumberscsv =~ $patientidfiltered ]]; then
			echo "OK: $patientidfiltered is in the list of valid test personal identity numbers.";
		else
			echo "ERROR: $patientidfiltered is not in the list of valid test personal identity numbers.";
			exitcode=1	
		fi
	fi 
done

exit $exitcode

 