#!/bin/bash

CI_SUPPORT=$(dirname $0)
ERRORS=0

find * -type d -prune | { \
  while read CONTRACT_NAME; do
    TEST_PROJECT="${CONTRACT_NAME}/${CONTRACT_NAME}-soapui-project.xml"
    MOCK_PROJECT="${CONTRACT_NAME}Mock/${CONTRACT_NAME}Mock-soapui-project.xml"
    if [ ! -f ${TEST_PROJECT} ] || [ ! -f ${MOCK_PROJECT} ] ; then
      continue
    fi

    /bin/bash "${CI_SUPPORT}/verify_testsuite.sh" "${TEST_PROJECT}"
    ERRORS=$(($ERRORS+$?))
  done

  echo "SUMMARY: Found errors in $ERRORS test-suites."
  exit $ERRORS
}
