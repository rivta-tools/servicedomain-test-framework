#!/bin/bash
source $(dirname $0)/runner.sh

java $JAVA_OPTS -cp $SOAPUI_CLASSPATH com.eviware.soapui.tools.SoapUILoadTestRunner "${@:2}"
echo