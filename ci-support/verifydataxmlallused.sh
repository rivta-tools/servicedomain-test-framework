#!/bin/bash

echo "--- Verifying that all test cases in $(basename $1) are used in $(basename $2). ---"

soapuixml=$(cat "$2")
IFS=$'\n'
testcases=($(grep -o -P '(?<=<testcase id=").*(?=")' "$1"))
unset IFS
exitcode=0

for i in ${!testcases[*]}
do
	testcaseid=${testcases[$i]}
	if [[ $soapuixml =~ $testcaseid ]]; then
		echo "OK: $testcaseid is used."
	else
		echo "ERROR: $testcaseid is not used in $2";
		exitcode=1
	fi 
done

exit $exitcode

 