#!/bin/bash
source $(dirname $0)/runner.sh

java $JAVA_OPTS -cp $SOAPUI_CLASSPATH com.eviware.soapui.tools.SoapUITestCaseRunner "${@:2}"
echo