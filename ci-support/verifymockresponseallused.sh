#!/bin/bash

echo "--- Verifying that all mock responses in $(basename $1) are used in $(basename $2). ---"

IFS=$'\n'
mockresponses=($(grep -o -P '(?<=response name=").*?(?=")' "$1"))
mockresponsesused=($(grep -o -P '(?<=entry key="x-mock-response" value=").*?(?=")' "$2"))
defaultmockresponse=($(grep -o -P '(?<=<con:defaultResponse>).*?(?=</con:defaultResponse>)' "$1")) 
allmockresponsesused=("${mockresponsesused[@]}" "${defaultmockresponse[@]}")
unset IFS
exitcode=0

for i in ${!mockresponses[*]}
do
	mockresponse=${mockresponses[$i]}
		
	if [[ " ${allmockresponsesused[@]} " =~ " ${mockresponse} " ]]; then
		echo "OK: $mockresponse is used."
	else
		echo "ERROR: $mockresponse is not used in $2 and it is not the default response for the mock.";
		exitcode=1
	fi
done

exit $exitcode

 