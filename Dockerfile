FROM openjdk:11-jre

ENV SOAPUI_VERSION=5.6.1

# Python and xsltproc is used in ci-support scripts
RUN apt-get update && apt-get install -y python xsltproc procps

RUN mkdir -p /opt \
 && curl https://s3.amazonaws.com/downloads.eviware/soapuios/${SOAPUI_VERSION}/SoapUI-${SOAPUI_VERSION}-linux-bin.tar.gz \
    | gunzip -c - | tar -xf - -C /opt \
 && ln -s /opt/SoapUI-${SOAPUI_VERSION} /opt/soapui

COPY soapui-support/build/libs/*.jar /opt/soapui/bin/ext
COPY ci-support /opt/ci-support

ENV PATH ${PATH}:/opt/soapui/bin:/opt/ci-support
